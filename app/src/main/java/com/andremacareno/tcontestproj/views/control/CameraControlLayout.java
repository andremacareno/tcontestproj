package com.andremacareno.tcontestproj.views.control;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.andremacareno.tcontestproj.AndroidUtilities;

/**
 * Created by andremacareno on 06/05/16.
 */
public class CameraControlLayout extends RelativeLayout implements CameraControl {
    public interface CameraController
    {
        void didCaptureRequested();
        void didPhotoSaveAllowed();
        void didModeSwitchRequested(CameraMode mode);
        void didLongtapRecordStartRequested();
        void didLongtapRecordStopRequested();
        void didVideoSaveAllowed();
        void didRecordStartRequested();
        void didRecordStopRequested();
        void didSwapCameraRequested();
        void didFlashModeChangeRequested(FlashMode mode);

        void onDismiss();
        void onAccept();

        boolean isRecording();
        boolean isLongtapRecording();
        boolean isProcessing();
        CameraMode getCurrentCameraMode();
    }
    private boolean clickEventLock = false;
    static Handler longtapCoordinator = new Handler();
    public CameraControlsBackground transparentBg;
    public SwapCameraButton swap;
    public CaptureButton capture;
    public CameraController controller;

    //decision
    public ImageButton actionButton;
    public SaveButton save;
    public DismissButton dismiss;

    /*private final Runnable showSwapRunnable = new Runnable() {
        @Override
        public void run() {
            if(swap != null)
                swap.show();
        }
    };*/
    private final Runnable longtapStartRec = new Runnable() {
        @Override
        public void run() {
            if(capture == null || swap == null || controller == null)
                return;
            clickEventLock = true;
            if(capture.didLongtapRecStarted()) {
                requestDisallowInterceptTouchEvent(true);
                hideTransparency();
                swap.hide();
                controller.didLongtapRecordStartRequested();
            }
        }
    };

    public CameraControlLayout(Context context) {
        super(context);
        init();
    }

    public CameraControlLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        setBackgroundColor(0x00000000);
        LayoutParams swapLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams captureLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams transparentBgLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        LayoutParams saveLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams dismissLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams additionalActionLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        additionalActionLP.addRule(CENTER_IN_PARENT);

        saveLP.addRule(CENTER_VERTICAL);
        saveLP.addRule(ALIGN_PARENT_RIGHT);
        dismissLP.addRule(CENTER_VERTICAL);
        dismissLP.addRule(ALIGN_PARENT_LEFT);
        swapLP.addRule(CENTER_VERTICAL);
        saveLP.rightMargin = AndroidUtilities.dp(32);
        dismissLP.leftMargin = swapLP.leftMargin = AndroidUtilities.dp(32);
        captureLP.addRule(CENTER_IN_PARENT);
        actionButton = new ImageButton(getContext());
        actionButton.setBackgroundColor(0x00000000);
        actionButton.setLayoutParams(additionalActionLP);
        actionButton.setVisibility(GONE);
        swap = new SwapCameraButton(getContext());
        swap.setLayoutParams(swapLP);

        capture = new CaptureButton(getContext());
        capture.setLayoutParams(captureLP);
        transparentBg = new CameraControlsBackground(getContext());
        transparentBg.setLayoutParams(transparentBgLP);

        save = new SaveButton(getContext());
        save.setVisibility(GONE);
        save.setLayoutParams(saveLP);
        dismiss = new DismissButton(getContext());
        dismiss.setVisibility(GONE);
        dismiss.setLayoutParams(dismissLP);



        addListeners();
        addView(transparentBg);
        addView(capture);
        addView(swap);
        addView(save);
        addView(dismiss);
        addView(actionButton);
    }
    public void showDecisionButtons()
    {
        capture.hide();
        swap.hide();
        save.show();
        dismiss.show();
        actionButton.setVisibility(VISIBLE);
    }
    public boolean hideDecisionButtons()
    {
        if(save.isAnimating() || dismiss.isAnimating())
            return false;
        swap.show();
        save.hide();
        dismiss.hide();
        capture.show();
        actionButton.setVisibility(GONE);
        return true;
    }
    public void showTransparency()
    {
        transparentBg.beginAnimation();
    }
    public void hideTransparency()
    {
        transparentBg.beginAnimation();
    }

    private void addListeners()
    {
        swap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(controller == null)
                    return;
                if(!controller.isRecording() && !controller.isProcessing()) {
                    if(swap.beginAnimation())
                        controller.didSwapCameraRequested();
                }
            }
        });

        capture.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(controller == null)
                    return false;
                if(controller.isProcessing())
                    return false;
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    if(controller.getCurrentCameraMode() != CameraMode.VIDEO && !controller.isProcessing())
                        longtapCoordinator.postDelayed(longtapStartRec, ViewConfiguration.getLongPressTimeout());
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if(controller.isLongtapRecording())
                    {
                        capture.didLongtapRecordStopped();
                        //longtapCoordinator.postDelayed(showSwapRunnable, 300);
                        showTransparency();
                        requestDisallowInterceptTouchEvent(false);
                        controller.didLongtapRecordStopRequested();
                    }
                    else
                        longtapCoordinator.removeCallbacksAndMessages(null);
                }
                return false;
            }
        });

        capture.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Controller", "click");
                if(controller == null)
                    return;
                if(controller.isProcessing())
                    return;
                if(clickEventLock)
                    return;
                if(controller.isLongtapRecording()) {
                    /*if(!clickEventLock)
                    {
                        clickEventLock = true;
                        controller.didLongtapRecordStopRequested();
                    }*/
                    return;
                }
                if(capture.isAnimating())
                    return;
                if(controller.getCurrentCameraMode() == CameraMode.VIDEO) {
                    if(controller.isRecording()) {
                        if(capture.didRecordStopped())
                        {
                            //sw.show();
                            showTransparency();
                            //animationCoordinator.postDelayed(showSwapRunnable, 300);
                            controller.didRecordStopRequested();
                        }
                    }
                    else
                    {
                        if(capture.didRecordStarted())
                        {
                            //sw.hide();
                            //animationCoordinator.removeCallbacksAndMessages(null);
                            swap.hide();
                            hideTransparency();
                            controller.didRecordStartRequested();
                        }
                    }
                }
                else
                {
                    if(capture.onCapture()) {
                        swap.hide();
                        controller.didCaptureRequested();
                    }
                }
            }
        });
        dismiss.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                performDismissAction();
            }
        });
        save.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //if(hideDecisionButtons() && controller != null)
                controller.onAccept();
            }
        });
    }
    public void performDismissAction()
    {
        if(hideDecisionButtons() && controller != null)
            controller.onDismiss();
    }
    public void allowClickEvent() { clickEventLock = false;}
    @Override
    public void setController(CameraController controller) {
        this.controller = controller;
        if(capture != null)
            capture.setController(controller);
    }
}
