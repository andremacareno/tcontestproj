package com.andremacareno.tcontestproj.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

import com.andremacareno.tcontestproj.AndroidUtilities;

/**
 * Created by Andre Macareno on 01.05.2016.
 */
public class CropView extends View implements View.OnTouchListener {
    private static final String TAG = "CropView";
    private Point screen = null;
    private Paint rectanglePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint linesPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint rectangleFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint cornersPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint fadePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Rect rectangleRegion = new Rect(0, 0, 0, 0);
    private Rect viewRegion = new Rect(0, 0, 0, 0);
    private int mRatioWidth = 0, mRatioHeight = 0;
    private final Path corners = new Path();
    private final Path fade = new Path();
    private final int cornerOffset;
    private final float cornerSize, cornerStrokeSize, touchDeltaMax;
    private EditorDelegate delegate;

    //touch statement
    float touchX, touchY;
    private boolean movingLeftEdge = false;
    private boolean movingTopEdge = false;
    private boolean movingRightEdge = false;
    private boolean movingBottomEdge = false;
    private int[] screenLocation = new int[2];

    public CropView(Context context) {
        super(context);
        cornerSize = AndroidUtilities.dp(17);
        cornerStrokeSize = AndroidUtilities.dp(3);
        touchDeltaMax = AndroidUtilities.dp(10);
        cornerOffset = AndroidUtilities.dp(3);
        init();
    }
    private void init()
    {
        rectanglePaint.setStrokeWidth(AndroidUtilities.dp(2));
        rectanglePaint.setColor(0xFFA9BBC5);
        rectanglePaint.setStyle(Paint.Style.STROKE);

        linesPaint.setStrokeWidth(AndroidUtilities.dp(1));
        linesPaint.setColor(0xFFA9BBC5);
        linesPaint.setStyle(Paint.Style.STROKE);

        rectangleFillPaint.setColor(0x00000000);
        rectangleFillPaint.setStyle(Paint.Style.FILL);

        cornersPaint.setColor(0xFFFFFFFF);
        cornersPaint.setStyle(Paint.Style.FILL);

        fadePaint.setColor(0x90000000);
        fadePaint.setStyle(Paint.Style.FILL);
        setOnTouchListener(this);
    }
    public void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            width = Math.abs(width);
            height = Math.abs(height);
        }
        if(screen == null) {
            screen = new Point();
            AndroidUtilities.getWindowManager().getDefaultDisplay().getSize(screen);
        }
        mRatioWidth = width;
        mRatioHeight = height;
        requestLayout();
    }

    public void rotate(float degree)
    {
        /*totalRotation += degree;
        int quadrant = (int) (Math.abs(totalRotation) / 90.0f);
        if(quadrant == previousQuadrant)
            return;
        previousQuadrant = quadrant;
        if(quadrant == 1 || quadrant == 3)
        {
            rectangleRegion.top = viewRegion.left + (viewRegion.width() / 2);
            rectangleRegion.bottom = viewRegion.right - (viewRegion.width() / 2);
        }
        else
        {
            rectangleRegion.top = viewRegion.top;
            rectangleRegion.bottom = viewRegion.bottom;
        }
        updateCorners();
        generateFadePath();
        postInvalidate();*/
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        getLocationOnScreen(screenLocation);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int w, h;
        /*if (0 == mRatioWidth || 0 == mRatioHeight) {
            w = width;
            h = height;
        } else {
            if (width < height * mRatioWidth / mRatioHeight) {
                width = Math.max(width, screen == null ? 0 : screen.x);
                w = width;
                h = height * mRatioHeight / mRatioWidth;
            } else {
                w = Math.min(height * mRatioHeight / mRatioHeight, screen == null ? 0 : screen.x);
                h = height;
            }
        }*/
        viewRegion.top = viewRegion.left = rectangleRegion.top = rectangleRegion.left = 0;
        viewRegion.right = rectangleRegion.right = width;
        viewRegion.bottom = rectangleRegion.bottom = height;
        updateCorners();
        generateFadePath();
        postInvalidate();
        setMeasuredDimension(width, height);
    }

    @Override
    public boolean onTouch(View view, MotionEvent e) {
        if(e.getPointerCount() > 1) {
            movingLeftEdge = movingRightEdge = movingBottomEdge = movingTopEdge = false;
            return false;
        }
        int act = e.getAction();
        float y = e.getRawY() - screenLocation[1];
        float x = e.getRawX() - screenLocation[0];
        /*if(y < screenLocation[1] || y > screenLocation[1]+getMeasuredHeight()) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("return; y = %f; location = %d", y, screenLocation[1]));
            return false;
        }
        if(x < screenLocation[0] || x > screenLocation[0]+getMeasuredWidth())
            return false;*/
        if(act == MotionEvent.ACTION_DOWN)
        {
            touchX = x;
            touchY = y;
            return touchingBottomCropEdge(touchX, touchY) || touchingTopCropEdge(touchX, touchY) || touchingLeftCropEdge(touchX, touchY) || touchingRightCropEdge(touchX, touchY);
        }
        if(act == MotionEvent.ACTION_UP || act == MotionEvent.ACTION_CANCEL)
        {
            movingLeftEdge = movingRightEdge = movingBottomEdge = movingTopEdge = false;
            postInvalidate();
            return false;
        }
        if(act == MotionEvent.ACTION_MOVE)
        {
            if(movingLeftEdge || touchingLeftCropEdge(x, y))
            {
                int width = rectangleRegion.width();
                int nextWidth = rectangleRegion.right - (int) x;
                if((nextWidth < width && nextWidth < 100) || (nextWidth > width && x < screenLocation[0]))
                    return false;
                movingLeftEdge = true;
                rectangleRegion.left = (int) x;
            }
            else if(movingRightEdge || touchingRightCropEdge(x, y))
            {
                int width = rectangleRegion.width();
                int nextWidth = (int) x - rectangleRegion.left;
                if((nextWidth < width && nextWidth < 100) || (nextWidth > width && x > screenLocation[0] + getMeasuredWidth()))
                    return false;
                movingRightEdge = true;
                rectangleRegion.right = (int) x;
            }
            else if(movingTopEdge || touchingTopCropEdge(x, y))
            {
                int height = rectangleRegion.height();
                int nextHeight = rectangleRegion.bottom - (int) y;
                if((nextHeight < height && nextHeight < 100) || (nextHeight > height && y < 0))
                    return false;
                movingTopEdge = true;
                rectangleRegion.top = (int) y;
            }
            else if(movingBottomEdge || touchingBottomCropEdge(x, y))
            {
                int height = rectangleRegion.height();
                int nextHeight = (int) y - rectangleRegion.top;
                if((nextHeight < height && nextHeight < 100) || (nextHeight > height && y > getMeasuredHeight()))
                    return false;
                movingBottomEdge = true;
                rectangleRegion.bottom = (int) y;
            }
            if(movingBottomEdge || movingTopEdge || movingLeftEdge || movingRightEdge)
            {
                if(delegate != null)
                    delegate.didCropMoved();
                updateCorners();
                generateFadePath();
                invalidate();
                return true;
            }
        }
        return false;
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        if(rectangleRegion.bottom == 0)
            return;
        canvas.drawARGB(0, 0, 0, 0);
        //canvas.setMatrix(m);
        canvas.drawPath(fade, fadePaint);
        canvas.drawRect(rectangleRegion, rectanglePaint);
        int xVertLine1 = rectangleRegion.left + rectangleRegion.width()/3;
        int xVertLine2 = rectangleRegion.left + rectangleRegion.width()*2/3;

        int yHorLine1 = rectangleRegion.top + rectangleRegion.height()/3;
        int yHorLine2 = rectangleRegion.top + rectangleRegion.height()*2/3;
        if(movingBottomEdge || movingRightEdge || movingLeftEdge || movingTopEdge)
        {
            canvas.drawLine(xVertLine1, rectangleRegion.top, xVertLine1, rectangleRegion.bottom, linesPaint);
            canvas.drawLine(xVertLine2, rectangleRegion.top, xVertLine2, rectangleRegion.bottom, linesPaint);
            canvas.drawLine(rectangleRegion.left, yHorLine1, rectangleRegion.right, yHorLine1, linesPaint);
            canvas.drawLine(rectangleRegion.left, yHorLine2, rectangleRegion.right, yHorLine2, linesPaint);
        }
        canvas.drawPath(corners, cornersPaint);
    }

    private void updateCorners()
    {

        corners.reset();
        corners.moveTo(rectangleRegion.left - cornerStrokeSize + cornerOffset, rectangleRegion.top - cornerStrokeSize + cornerOffset);
        corners.rLineTo(cornerSize, 0);
        corners.rLineTo(0, cornerStrokeSize);
        corners.rLineTo(cornerStrokeSize - cornerSize, 0);
        corners.rLineTo(0, cornerSize - cornerStrokeSize);
        corners.rLineTo(-cornerStrokeSize, 0);
        corners.close();

        corners.moveTo(rectangleRegion.right + cornerStrokeSize - cornerOffset, rectangleRegion.top - cornerStrokeSize + cornerOffset);
        corners.rLineTo(0, cornerSize);
        corners.rLineTo(-cornerStrokeSize, 0);
        corners.rLineTo(0, cornerStrokeSize-cornerSize);
        corners.rLineTo(cornerStrokeSize-cornerSize, 0);
        corners.rLineTo(0, -cornerStrokeSize);
        corners.close();

        corners.moveTo(rectangleRegion.right + cornerStrokeSize - cornerOffset, rectangleRegion.bottom + cornerStrokeSize - cornerOffset);
        corners.rLineTo(-cornerSize, 0);
        corners.rLineTo(0, -cornerStrokeSize);
        corners.rLineTo(cornerSize-cornerStrokeSize, 0);
        corners.rLineTo(0, cornerStrokeSize-cornerSize);
        corners.rLineTo(cornerStrokeSize, 0);
        corners.close();

        corners.moveTo(rectangleRegion.left - cornerStrokeSize + cornerOffset, rectangleRegion.bottom + cornerStrokeSize - cornerOffset);
        corners.rLineTo(0, -cornerSize);
        corners.rLineTo(cornerStrokeSize, 0);
        corners.rLineTo(0, (cornerSize - cornerStrokeSize));
        corners.rLineTo((cornerSize - cornerStrokeSize), 0);
        corners.rLineTo(0, cornerStrokeSize);
        corners.close();
    }

    private void generateFadePath()
    {
        fade.reset();
        fade.moveTo(viewRegion.left, viewRegion.top);
        fade.rLineTo(0, viewRegion.bottom-viewRegion.top);
        fade.rLineTo(rectangleRegion.left-viewRegion.left, 0);
        fade.rLineTo(0, rectangleRegion.top - viewRegion.bottom);
        fade.rLineTo(rectangleRegion.right-rectangleRegion.left, 0);
        fade.rLineTo(0, viewRegion.bottom - rectangleRegion.top);
        fade.rLineTo(viewRegion.right - rectangleRegion.right, 0);
        fade.rLineTo(0, viewRegion.top - viewRegion.bottom);

        fade.moveTo(rectangleRegion.left, rectangleRegion.bottom);
        fade.addRect(rectangleRegion.left, rectangleRegion.bottom, rectangleRegion.right, viewRegion.bottom, Path.Direction.CW);
        fade.close();
    }

    private boolean touchingLeftCropEdge(float x, float y)
    {
        float dx = Math.abs(x - rectangleRegion.left);
        boolean insideRect = y >= rectangleRegion.top && y <= rectangleRegion.bottom;
        return dx <= touchDeltaMax && insideRect;
    }
    private boolean touchingRightCropEdge(float x, float y)
    {
        float dx = Math.abs(x - rectangleRegion.right);
        boolean insideRect = y >= rectangleRegion.top && y <= rectangleRegion.bottom;
        return dx <= touchDeltaMax && insideRect;
    }
    private boolean touchingTopCropEdge(float x, float y)
    {
        float dy = Math.abs(y - rectangleRegion.top);
        boolean insideRect = x >= rectangleRegion.left && x <= rectangleRegion.right;
        return dy <= touchDeltaMax && insideRect;
    }
    private boolean touchingBottomCropEdge(float x, float y)
    {
        float dy = Math.abs(y - rectangleRegion.bottom);
        boolean insideRect = x >= rectangleRegion.left && x <= rectangleRegion.right;
        return dy <= touchDeltaMax && insideRect;
    }
    public Rect getCropRegion() { return rectangleRegion; }
    public void setDelegate(EditorDelegate delegate) { this.delegate = delegate; }

}
