package com.andremacareno.tcontestproj.views;

/**
 * Created by Andre Macareno on 10.05.2016.
 */
public interface EditorDelegate {
    void didCancelRequested();
    void didResetRequested();
    void didRotateRequested();
    void didFinishRequested();
    void didPhotoRectChanged();
    void didPhotoRectNormalized();
    void didCropMoved();
}
