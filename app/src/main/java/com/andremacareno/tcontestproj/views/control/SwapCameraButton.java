package com.andremacareno.tcontestproj.views.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.R;

/**
 * Created by andremacareno on 04/05/16.
 */
public class SwapCameraButton extends View {
    private int circleRadius;
    private Paint circlePaint;
    private Drawable swapDrawable;
    private static final int ANIM_DURATION = 192;
    private static final int ALPHA_DURATION = 150;
    private static final int DELAY = 16;
    private int time, alphaTime;

    private int centerX = -1, centerY = -1;
    private float k = 0.0f;
    private float alphaK = 1.0f;
    boolean animating = false;
    boolean animatingAlpha = false;
    boolean reverse = false;


    private final Runnable showRunnable = new Runnable() {
        @Override
        public void run() {
            alphaK = getProgress(alphaTime, 0, 1, ALPHA_DURATION);
            if(alphaK > 1.0f)
                alphaK = 1.0f;
            alphaTime += DELAY;
            postInvalidate();
            if(alphaTime < ALPHA_DURATION)
                postDelayed(this, DELAY);
            else {
                animatingAlpha = false;
            }
        }
    };
    private final Runnable hideRunnable = new Runnable() {
        @Override
        public void run() {
            alphaK = getProgress(alphaTime, 1, -1, ALPHA_DURATION);
            if(alphaK < 0.0f)
                alphaK = 0.0f;
            alphaTime += DELAY;
            postInvalidate();
            if(alphaTime < ALPHA_DURATION)
                postDelayed(this, DELAY);
            else {
                setVisibility(GONE);
                animatingAlpha = false;
            }
        }
    };
    private final Runnable rotateRunnable = new Runnable() {
        @Override
        public void run() {
            k = reverse ? getProgress(time, 1, -1, ANIM_DURATION) : getProgress(time, 0, 1, ANIM_DURATION);
            if(k > 1.0f)
                k = 1.0f;
            else if(k < 0.0f)
                k = 0.0f;
            time += DELAY;
            postInvalidate();
            if(time < ANIM_DURATION)
                postDelayed(this, DELAY);
            else {
                time = 0;
                k = reverse ? 0.0f : 1.0f;
                animating = false;
            }
        }
    };

    public SwapCameraButton(Context context) {
        super(context);
        init();
    }

    public SwapCameraButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        swapDrawable = ContextCompat.getDrawable(getContext(), R.drawable.swapcamera);
        swapDrawable.setBounds(0, 0, swapDrawable.getIntrinsicWidth(), swapDrawable.getIntrinsicHeight());
        circleRadius = AndroidUtilities.dp(7);
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setColor(0xFFFFFFFF);
        circlePaint.setStyle(Paint.Style.STROKE);
        circlePaint.setStrokeWidth(AndroidUtilities.dp(2));

        ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(Build.VERSION.SDK_INT >= 16)
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                centerX = getMeasuredWidth() / 2;
                centerY = getMeasuredHeight() / 2;
                postInvalidate();
            }
        };
        getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        if(swapDrawable != null)
            setMeasuredDimension(swapDrawable.getIntrinsicWidth(), swapDrawable.getIntrinsicHeight());
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        if(centerX <= 0 || centerY <= 0)
            return;
        canvas.drawARGB(0, 0, 0, 0);
        int sign = reverse ? -1 : 1;
        canvas.rotate(-180.0f * k * sign, centerX, centerY);
        swapDrawable.setAlpha((int) (255 * alphaK));
        swapDrawable.draw(canvas);
        float strokeWidth = AndroidUtilities.dp(2) + AndroidUtilities.dp(5) * k;
        circlePaint.setStrokeWidth(strokeWidth);
        circlePaint.setColor(Color.argb((int) (255 * alphaK), 0xFF, 0xFF, 0xFF));
        canvas.drawCircle(centerX, centerY, circleRadius - strokeWidth/2, circlePaint);

    }
    public boolean beginAnimation()
    {
        if(animating || animatingAlpha)
            return false;
        animating = true;
        time = 0;
        reverse = k > 0.0f;
        postDelayed(rotateRunnable, DELAY);
        return true;
    }

    //Use if you have properly show/hide animation idea.
    public void show()
    {
        if(animatingAlpha)
            return;
        animatingAlpha = true;
        alphaTime = 0;
        setVisibility(VISIBLE);
        postDelayed(showRunnable, DELAY);
    }
    public void hide()
    {
        if(animatingAlpha) {
            return;
        }
        animatingAlpha = true;
        alphaTime = 0;
        postDelayed(hideRunnable, DELAY);
    }
    //easeOutQuart
    private static float getProgress(float time, float start, float delta, float duration)
    {
        time /= duration;
        time--;
        return -delta * (time * time * time * time - 1) + start;
    }
    public boolean isAnimating()
    {
        return animating;
    }
}
