package com.andremacareno.tcontestproj.views.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.BuildConfig;
import com.andremacareno.tcontestproj.R;

import java.util.Locale;

/**
 * Created by andremacareno on 04/05/16.
 */
public class RecordingTimer extends View {
    private static final int ANIM_DURATION = 300;
    private static final int DELAY = 16;
    private long startTime = 0;
    private String stringTime = "";
    private int animTime;
    private boolean running = false;
    private float k = 0.0f;
    private boolean reverse = false;
    private final int radius;
    boolean animating = false;
    private int paddingTop, paddingLeft;
    private Paint rectPaint;
    private TextPaint txtPaint;
    private final RectF bgRect = new RectF();
    private final Rect textBounds = new Rect();
    private static final Handler tickHandler = new Handler(Looper.getMainLooper());
    private static final Handler animHandler = new Handler(Looper.getMainLooper());
    private final Runnable tickRunnable = new Runnable() {
        @Override
        public void run() {
            long delta = (System.currentTimeMillis() - startTime) / 1000;
            stringTime = String.format(Locale.US, "%02d:%02d", delta / 60, delta % 60);
            postInvalidate();
            tickHandler.postDelayed(this, 1000);
        }
    };

    private Runnable translateRunnable = new Runnable() {
        @Override
        public void run() {
            k = reverse ? getProgress(animTime, 1, -1, ANIM_DURATION) : getProgress(animTime, 0, 1, ANIM_DURATION);
            if(k > 1.0f)
                k = 1.0f;
            if(k < 0.0f)
                k = 0.0f;
            animTime += DELAY;
            postInvalidate();
            if(animTime < ANIM_DURATION)
                animHandler.postDelayed(this, DELAY);
            else {
                if(running && reverse) {
                    running = false;
                    setVisibility(GONE);
                }
                animating = false;
            }
        }
    };

    public RecordingTimer(Context context) {
        super(context);
        radius = AndroidUtilities.dp(6);
        init();
    }

    private void init()
    {
        paddingTop = AndroidUtilities.dp(8);
        paddingLeft = AndroidUtilities.dp(16);
        txtPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        txtPaint.setTypeface(Typeface.DEFAULT_BOLD);
        txtPaint.setTextSize(AndroidUtilities.dp(14));
        txtPaint.setColor(0xFFFFFFFF);
        rectPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        rectPaint.setColor(0x66000000);
        rectPaint.setStyle(Paint.Style.FILL);


        ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(Build.VERSION.SDK_INT >= 16)
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                postInvalidate();
            }
        };
        getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
    }
    /*public FlashMode nextMode()
    {
        if(animating)
            return null;
        animating = true;
        previousMode = mode;
        if(mode == FlashMode.AUTO)
            mode = FlashMode.ON;
        else if(mode == FlashMode.ON)
            mode = FlashMode.OFF;
        else
            mode = FlashMode.AUTO;
        performTranslate();
        return mode;
    }*/
    public void show() {
        running = true;
        reverse = false;
        stringTime = "00:00";
        startTime = System.currentTimeMillis();
        animTime = 0;
        animHandler.postDelayed(translateRunnable, DELAY);
        setVisibility(VISIBLE);
        tickHandler.post(tickRunnable);
    }
    public void hide() {
        animTime = 0;
        tickHandler.removeCallbacksAndMessages(null);
        animHandler.removeCallbacksAndMessages(null);
        reverse = true;
        animHandler.postDelayed(translateRunnable, DELAY);
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        if(getMeasuredWidth() <= 0 || !running)
            return;
        canvas.drawARGB(0, 0, 0, 0);
        txtPaint.getTextBounds(stringTime, 0, stringTime.length(), textBounds);
        if(BuildConfig.DEBUG)
            Log.d("Timer", String.format("textWidth = %d", textBounds.width()));
        int textWidth = textBounds.width();
        bgRect.left = (getMeasuredWidth() - textWidth) / 2 - paddingLeft;
        bgRect.top = (getMeasuredHeight() - textBounds.height()) / 2 - paddingTop;
        bgRect.right = (getMeasuredWidth() + textWidth) / 2 + paddingLeft;
        bgRect.bottom = (getMeasuredHeight() + textBounds.height()) / 2 + paddingTop;

        bgRect.top -= getMeasuredHeight() * (1.0f - k);
        bgRect.bottom -= getMeasuredHeight() * (1.0f - k);
        canvas.drawRoundRect(bgRect, radius, radius, rectPaint);
        canvas.drawText(stringTime, bgRect.left + paddingLeft, bgRect.top + bgRect.height()/2 + textBounds.height() / 2, txtPaint);
    }

    //easeOutQuart
    private static float getProgress(float time, float start, float delta, float duration)
    {
        time /= duration;
        time--;
        return -delta * (time * time * time * time - 1) + start;
    }
}
