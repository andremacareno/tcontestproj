package com.andremacareno.tcontestproj.views;

import android.content.Context;
import android.widget.RelativeLayout;

/**
 * Created by andremacareno on 27/04/16.
 */
public class CameraView extends RelativeLayout {
    public AutoFitTextureView textureView;
    public CameraView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        setBackgroundColor(0xFF25272A);
        RelativeLayout.LayoutParams tvLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams takePictureLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams swapCameraLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        takePictureLP.addRule(RelativeLayout.CENTER_HORIZONTAL);
        takePictureLP.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        swapCameraLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        swapCameraLP.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        textureView = new AutoFitTextureView(getContext());
        textureView.setLayoutParams(tvLP);
        textureView.setVisibility(VISIBLE);

        addView(textureView);
    }

}
