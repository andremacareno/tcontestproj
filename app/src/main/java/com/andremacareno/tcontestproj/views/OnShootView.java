package com.andremacareno.tcontestproj.views;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.BuildConfig;
import com.andremacareno.tcontestproj.R;

/**
 * Created by andremacareno on 27/04/16.
 */
public class OnShootView extends RelativeLayout {
    public ImageView imageView;
    public CropView cropView;

    /*public ImageButton saveButton;
    public ImageButton discardButton;
    public ImageButton editButton;*/
    public ImageButton rotateButton;

    public Button cancelButton;
    public Button doneButton;
    public Button resetButton;

    private RelativeLayout editLayout, rotateLayout;
    private EditorDelegate delegate;

    public OnShootView(Context context) {
        super(context);
        init();
    }
    private void init()
    {
        setBackgroundColor(0xFF000000);
        editLayout = new RelativeLayout(getContext());
        editLayout.setBackgroundColor(0xFF1D1E1F);
        editLayout.setId(R.id.edit_layout);
        rotateLayout = new RelativeLayout(getContext());
        rotateLayout.setBackgroundColor(0x00000000);
        rotateLayout.setId(R.id.rotate);
        LayoutParams subLayoutLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        subLayoutLP.addRule(ALIGN_PARENT_BOTTOM);

        LayoutParams rotateLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        rotateLP.addRule(ABOVE, R.id.edit_layout);
        rotateLayout.setLayoutParams(rotateLP);


        int rotateBtnMargin = AndroidUtilities.dp(8);
        LayoutParams ivLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        LayoutParams saveLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams rotateBtnLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        rotateBtnLP.setMargins(rotateBtnMargin, rotateBtnMargin, rotateBtnMargin, rotateBtnMargin);
        LayoutParams discardLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams editLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams cancelLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams resetLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams doneLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        LayoutParams cropLP = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        rotateBtnLP.addRule(ALIGN_PARENT_RIGHT);
        cropLP.addRule(ALIGN_PARENT_TOP);
        cropLP.addRule(ALIGN_BOTTOM, R.id.photo);

        resetLP.addRule(RIGHT_OF, R.id.cancel_edit);
        ivLP.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        ivLP.addRule(ABOVE, R.id.rotate);
        editLP.addRule(RelativeLayout.CENTER_HORIZONTAL);
        editLP.addRule(RelativeLayout.CENTER_VERTICAL);

        saveLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

        discardLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

        cancelLP.addRule(ALIGN_PARENT_LEFT);
        doneLP.addRule(ALIGN_PARENT_RIGHT);
        imageView = new AutoFitPhotoView(getContext());
        imageView.setId(R.id.photo);
        cropView = new CropView(getContext());
        cropView.setLayoutParams(cropLP);
        imageView.setLayoutParams(ivLP);
        imageView.setVisibility(VISIBLE);

        cancelButton = new Button(getContext());
        cancelButton.setBackgroundColor(0x00000000);
        cancelButton.setAllCaps(true);
        cancelButton.setTextColor(0xFFFFFFFF);
        cancelButton.setId(R.id.cancel_edit);

        doneButton = new Button(getContext());
        doneButton.setBackgroundColor(0x00000000);
        doneButton.setAllCaps(true);
        doneButton.setTextColor(0xFF70BEF9);
        doneButton.setId(R.id.done_edit);

        resetButton = new Button(getContext());
        resetButton.setBackgroundColor(0x00000000);
        resetButton.setAllCaps(true);
        resetButton.setTextColor(0xFFFFFFFF);

        rotateButton = new ImageButton(getContext());
        rotateButton.setBackgroundResource(R.drawable.btn_selector);
        rotateButton.setImageResource(R.drawable.rotate);
        rotateButton.setLayoutParams(rotateBtnLP);


        cancelButton.setText(R.string.cancel);
        cancelButton.setBackgroundResource(R.drawable.btn_selector);
        doneButton.setText(R.string.done);
        doneButton.setBackgroundResource(R.drawable.btn_selector);
        resetButton.setText(R.string.reset);
        resetButton.setBackgroundResource(R.drawable.btn_selector);

        cancelButton.setLayoutParams(cancelLP);
        resetButton.setLayoutParams(resetLP);
        doneButton.setLayoutParams(doneLP);

        editLayout.setLayoutParams(subLayoutLP);
        addView(imageView);
        addView(cropView);

        editLayout.addView(cancelButton);
        editLayout.addView(doneButton);
        editLayout.addView(resetButton);

        rotateLayout.addView(rotateButton);

        cropView.setVisibility(GONE);
        editLayout.setVisibility(INVISIBLE);
        rotateLayout.setVisibility(INVISIBLE);
        addView(editLayout);
        addView(rotateLayout);

        cancelButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("OnShoot", "click");
                if(delegate != null)
                    delegate.didCancelRequested();
            }
        });
        resetButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(delegate != null)
                    delegate.didResetRequested();
            }
        });
        doneButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(delegate != null)
                    delegate.didFinishRequested();
            }
        });
        rotateButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(delegate != null)
                    delegate.didRotateRequested();
            }
        });
    }
    public void setEditMode(boolean editMode)
    {
        if(BuildConfig.DEBUG)
            Log.d("OnShootView", "setEditMode()");
        if(editMode) {
            editLayout.setVisibility(VISIBLE);
            rotateLayout.setVisibility(VISIBLE);
            cropView.setVisibility(VISIBLE);
        }
        else
        {
            if(BuildConfig.DEBUG)
                Log.d("OnShootView", "disabling edit mode");
            editLayout.setVisibility(INVISIBLE);
            cropView.setVisibility(GONE);
            rotateLayout.setVisibility(INVISIBLE);
        }
    }
    public void setEditorDelegate(EditorDelegate delegateRef) {
        this.delegate = delegateRef;
        cropView.setDelegate(delegate);
    }
}
