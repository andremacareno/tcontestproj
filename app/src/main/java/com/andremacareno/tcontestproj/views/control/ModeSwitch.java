package com.andremacareno.tcontestproj.views.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tcontestproj.AndroidUtilities;

/**
 * Created by andremacareno on 04/05/16.
 */
public class ModeSwitch extends View {
    public interface Callback
    {
        void onLeftSide();
        void onRightSide();
    }
    public enum Direction {TO_RIGHT, TO_LEFT}
    private static final int INACTIVE = 0x4CFFFFFF;
    private static final int ACTIVE = 0xBAFFFFFF;
    private static final int DELAY = 16;
    private int leftCenterX = 0, rightCenterX = 0, midCenterX = -1, radius = 0;
    private Paint activePaint, inactivePaint;
    private int height = 0;
    private final RectF switchOval = new RectF();
    private final RectF halfOval1 = new RectF();
    private final RectF halfOval2 = new RectF();
    private Direction dir = Direction.TO_RIGHT;
    private boolean moving = false;
    private float k = 0.0f, fadeK = 0.0f;
    private static final int ANIM_FULL_DURATION = 150;
    private static final int IDLE_FULL_DURATION = 100;
    private static final int FADE_DURATION = 200;
    private int duration = 0;
    private int rectTime = 0;
    private int fadeTime;
    private boolean moveAnimation = false;
    private boolean idleAnimation = false;
    private boolean rectAnimation = false;
    private boolean fadeAnimation = false;
    private Callback callback;
    private final Runnable moveRunnable = new Runnable() {
        @Override
        public void run() {
            float k1 = (float) duration / ANIM_FULL_DURATION;
            if(k1 > 1.0f)
                k1 = 1.0f;
            if(dir == Direction.TO_LEFT)
                midCenterX = rightCenterX - (int) ((rightCenterX - leftCenterX) * k1);
            else if(dir == Direction.TO_RIGHT)
                midCenterX = (int) ((rightCenterX - leftCenterX) * k1) + leftCenterX;
            if(duration <= ANIM_FULL_DURATION) {
                postInvalidate();
                duration += DELAY;
                postDelayed(this, DELAY);
            }
            else {
                if(callback != null) {
                    if(dir == Direction.TO_RIGHT)
                        callback.onRightSide();
                    else
                        callback.onLeftSide();
                }
                dir = dir == Direction.TO_RIGHT ? Direction.TO_LEFT : Direction.TO_RIGHT;
                moveAnimation = false;
                moving = false;
                postInvalidate();
                continueAnimation();
            }
        }
    };
    private final Runnable fadeInRunnable = new Runnable() {
        @Override
        public void run() {
            fadeK = getAlpha(fadeTime, 1, -1, FADE_DURATION);
            if(fadeK < 0.0f)
                fadeK = 0.0f;
            fadeTime += DELAY;
            if(fadeTime <= FADE_DURATION && fadeAnimation) {
                postInvalidate();
                postDelayed(this, DELAY);
            }
            else {
                fadeK = 0.0f;
                fadeAnimation = false;
            }
        }
    };
    private final Runnable fadeOutRunnable  = new Runnable() {
        @Override
        public void run() {
            fadeK = getAlpha(fadeTime, 0, 1, FADE_DURATION);
            if(fadeK > 1.0f)
                fadeK = 1.0f;
            fadeTime += DELAY;
            if(fadeTime <= FADE_DURATION && fadeAnimation) {
                postInvalidate();
                postDelayed(this, DELAY);
            }
            else {
                fadeK = 1.0f;
                fadeAnimation = false;
                setVisibility(GONE);
            }
        }
    };
    private final Runnable continueRunnable = new Runnable() {
        @Override
        public void run() {
            postInvalidate();
            rectTime += DELAY;
            if(rectTime > ANIM_FULL_DURATION)
                rectAnimation = false;
            else
                postDelayed(this, DELAY);
        }
    };
    private final Runnable idleRunnable = new Runnable() {
        @Override
        public void run() {
            idleAnimation = false;
            rectAnimation = true;
            rectTime = 0;
            postDelayed(continueRunnable, DELAY);
        }
    };
    public ModeSwitch(Context context) {
        super(context);
        init();
    }

    public ModeSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public void setCallback(Callback callback)
    {
        this.callback = callback;
    }
    private void init()
    {
        radius = AndroidUtilities.dp(4);
        activePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        activePaint.setStyle(Paint.Style.FILL);
        inactivePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        inactivePaint.setStyle(Paint.Style.FILL);
        ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(Build.VERSION.SDK_INT >= 16)
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                int centerOffset= AndroidUtilities.dp(8);
                int centerX = getMeasuredWidth() / 2;
                leftCenterX = centerX - centerOffset;
                rightCenterX = centerX + centerOffset;
                if(height == 0)
                    height = getMeasuredHeight() / 2;
                halfOval1.top = halfOval2.top = switchOval.top = height - radius;
                halfOval1.bottom = halfOval2.bottom = switchOval.bottom = height + radius;
                halfOval1.left = leftCenterX - radius;
                halfOval2.right = rightCenterX + radius;
                postInvalidate();
            }
        };
        getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        setMeasuredDimension(MeasureSpec.getSize(measureWidthSpec), radius*2);
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        if(getMeasuredWidth() == 0 || getMeasuredHeight() == 0 || radius <= 0)
            return;
        canvas.drawARGB(0, 0, 0, 0);
        activePaint.setColor(ACTIVE);
        inactivePaint.setColor(INACTIVE);
        if(moving) {
            if(midCenterX > 0)
            {
                switchOval.left = dir == Direction.TO_RIGHT ? leftCenterX - radius : midCenterX - radius;
                switchOval.right = dir == Direction.TO_RIGHT ? midCenterX + radius : rightCenterX + radius;
                canvas.drawRoundRect(switchOval, radius, radius, inactivePaint);
                canvas.drawCircle(midCenterX, height, radius, activePaint);
            }
        }
        else
        {
            if(idleAnimation)
                canvas.drawRoundRect(switchOval, radius, radius, inactivePaint);
            else if(rectAnimation)
            {
                int center = (int) (switchOval.left + switchOval.right) / 2;
                halfOval1.right = getRectWidth(rectTime, center, -radius, ANIM_FULL_DURATION);
                halfOval2.left = getRectWidth(rectTime, center, radius, ANIM_FULL_DURATION);
                canvas.drawRoundRect(halfOval1, radius, radius, inactivePaint);
                canvas.drawRoundRect(halfOval2, radius, radius, inactivePaint);
            }
        }
        if(!rectAnimation && !idleAnimation)
        {
            if(fadeAnimation)
            {
                inactivePaint.setColor(Color.argb((int) (0x4C * (1.0f - fadeK)), 0xFF, 0xFF, 0xFF));
                activePaint.setColor(Color.argb((int) (0xBA * (1.0f - fadeK)), 0xFF, 0xFF, 0xFF));
            }
            if(!(dir == Direction.TO_RIGHT && moving))
                canvas.drawCircle(leftCenterX, height, radius, dir == Direction.TO_RIGHT && !moving ? activePaint : inactivePaint);
            if(!(dir == Direction.TO_LEFT && moving))
                canvas.drawCircle(rightCenterX, height, radius, dir == Direction.TO_LEFT && !moving ? activePaint : inactivePaint);
        }
        else
        {
            if(dir == Direction.TO_LEFT)
                canvas.drawCircle(rightCenterX, height, radius, activePaint);
            else
                canvas.drawCircle(leftCenterX, height, radius, activePaint);
        }
    }
    public void move(int touchX, int x)
    {
        if(isAnimating())
            return;
        if(!checkDir(touchX, x))
            return;
        moving = true;
        if(dir == Direction.TO_RIGHT)
        {
            k = (((float) (touchX - x)) / ((getMeasuredWidth() / 4.0f)));
            if(k > 1.0f)
                k = 1.0f;
            midCenterX = (int) ((rightCenterX - leftCenterX) * k) + leftCenterX;
            postInvalidate();
        }
        else if(dir == Direction.TO_LEFT)
        {
            k = (((float) (x - touchX)) / ((getMeasuredWidth() / 4.0f)));
            if(k > 1.0f)
                k = 1.0f;
            midCenterX = rightCenterX - (int) ((rightCenterX - leftCenterX) * k);
            postInvalidate();
        }
    }
    public void onFinishMove(int touchX, int x)
    {
        if(isAnimating())
            return;
        if(!checkDir(touchX, x))
            return;
        if(k == 1.0f)
        {
            if(callback != null) {
                if(dir == Direction.TO_RIGHT)
                    callback.onRightSide();
                else
                    callback.onLeftSide();
            }
            dir = dir == Direction.TO_RIGHT ? Direction.TO_LEFT : Direction.TO_RIGHT;
            k = 0;
            moving = false;
            continueAnimation();
        }
        else if(k >= 0.5f)
            animateMove(touchX, x);
        else
        {
            moving = false;
            postInvalidate();
        }
    }


    public void animateMove(int touchX, int x0)
    {
        if(isAnimating())
            return;
        if(!checkDir(touchX, x0))
            return;
        duration = (int) (ANIM_FULL_DURATION * k);
        moveAnimation = true;
        moving = true;

        postDelayed(moveRunnable, DELAY);
    }

    private boolean checkDir(int touchX, int x0)
    {
        if(dir == Direction.TO_RIGHT) {
            if (touchX < x0)
                return false;
        }
        else
        {
            if(touchX > x0)
                return false;
        }
        return true;
    }
    private void continueAnimation()
    {

        idleAnimation = true;
        postDelayed(idleRunnable, IDLE_FULL_DURATION);
    }
    private void performFadeOut()
    {
        if(fadeAnimation)
            return;
        fadeAnimation = true;
        fadeTime = 0;
        postDelayed(fadeOutRunnable, DELAY);
    }
    private void performFadeIn()
    {
        if(fadeAnimation)
            return;
        fadeAnimation = true;
        fadeTime = 0;
        postDelayed(fadeInRunnable, DELAY);
    }
    public boolean hide()
    {
        if(isAnimating()) {
            setVisibility(GONE);
            return false;
        }
        performFadeOut();
        return true;
    }
    public boolean show()
    {
        setVisibility(VISIBLE);
        if(isAnimating())
            return false;
        performFadeIn();
        return true;
    }
    private boolean isAnimating()
    {
        return moveAnimation || idleAnimation || rectAnimation || fadeAnimation;
    }

    private static float getAlpha(float time, float start, float delta, float duration)
    {
        time /= duration;
        time--;
        return -delta * (time * time * time * time - 1) + start;
    }
    private static float getRectWidth(int time, int startWidth, int deltaW, int duration)
    {
        //return (int) (deltaW * Math.pow(2, 10 * (((double) time/duration - 1.0))) + startWidth);
        return deltaW * time / duration + startWidth;
    }

}
