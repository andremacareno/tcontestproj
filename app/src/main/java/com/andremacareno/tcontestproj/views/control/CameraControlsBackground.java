package com.andremacareno.tcontestproj.views.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created by andremacareno on 04/05/16.
 */
public class CameraControlsBackground extends View {
    private Paint bgPaint;
    private Rect background = new Rect();
    private static final int ANIM_DURATION = 300;
    private static final int DELAY = 16;
    private int time;

    private float k = 0.0f;
    boolean animating = false;
    boolean reverse = false;

    private final Runnable animRunnable = new Runnable() {
        @Override
        public void run() {
            k = reverse ? getProgress(time, 1, -1, ANIM_DURATION) : getProgress(time, 0, 1, ANIM_DURATION);
            if(k > 1.0f)
                k = 1.0f;
            else if(k < 0.0f)
                k = 0.0f;
            time += DELAY;
            postInvalidate();
            if(time < ANIM_DURATION)
                postDelayed(this, DELAY);
            else {
                time = 0;
                k = reverse ? 0.0f : 1.0f;
                animating = false;
            }
        }
    };

    public CameraControlsBackground(Context context) {
        super(context);
        init();
    }

    public CameraControlsBackground(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bgPaint.setColor(0x66000000);
        bgPaint.setStyle(Paint.Style.FILL);

        ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(Build.VERSION.SDK_INT >= 16)
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                background.top = 0;
                background.bottom = getMeasuredHeight();
                background.left = 0;
                background.right = getMeasuredWidth();
                postInvalidate();
            }
        };
        getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        if(getMeasuredWidth() == 0 || getMeasuredHeight() == 0)
            return;
        canvas.drawARGB(0, 0, 0, 0);
        background.top = (int) (getMeasuredHeight() * k);
        canvas.drawRect(background, bgPaint);
    }
    public void beginAnimation()
    {
        if(isAnimating())
            return;
        animating = true;
        time = 0;
        reverse = k > 0.0f;
        postDelayed(animRunnable, DELAY);
    }
    //easeInOutExpo
    private static float getProgress(float time, float start, float delta, float duration)
    {
        time /= duration/2;
        if (time < 1)
            return (float) (delta/2 * Math.pow( 2, 10 * (time - 1) ) + start);
        time--;
        return (float) (delta/2 * ( -Math.pow( 2, -10 * time) + 2 ) + start);
    }
    public boolean isAnimating()
    {
        return animating;
    }
}
