package com.andremacareno.tcontestproj.views.control;

/**
 * Created by andremacareno on 07/05/16.
 */
public enum CameraMode {STILL, VIDEO}
