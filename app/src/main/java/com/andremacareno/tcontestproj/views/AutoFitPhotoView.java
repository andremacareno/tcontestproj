/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.andremacareno.tcontestproj.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.TextureView;

import com.andremacareno.tcontestproj.utils.FastBitmapDrawable;

import uk.co.senab.photoview.PhotoView;

/**
 * A {@link TextureView} that can be adjusted to a specified aspect ratio.
 */
public class AutoFitPhotoView extends PhotoView {

    private int mRatioWidth = 0;
    private int mRatioHeight = 0;

    public AutoFitPhotoView(Context context) {
        super(context);
    }

    @Override
    public void setImageDrawable(Drawable drawable)
    {
        //Drawable previousDrawable = getDrawable();
        super.setImageDrawable(drawable);
        /*if(drawable == null && previousDrawable != null && previousDrawable instanceof FastBitmapDrawable)
            ((FastBitmapDrawable) previousDrawable).getBitmap().recycle();*/
        if(drawable != null && drawable instanceof FastBitmapDrawable) {
            setAspectRatio(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        }
    }
    public void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            throw new IllegalArgumentException("Size cannot be negative.");
        }
        mRatioWidth = width;
        mRatioHeight = height;
        requestLayout();
    }
    public float getAspectRatio()
    {
        return (float) mRatioWidth / (float) mRatioHeight;
    }

    /*@Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (0 == mRatioWidth || 0 == mRatioHeight) {
            setMeasuredDimension(width, height);
        } else {
            if (width < height * mRatioWidth / mRatioHeight) {
                setMeasuredDimension(width, width * mRatioHeight / mRatioWidth);
            } else {
                setMeasuredDimension(height * mRatioWidth / mRatioHeight, height);
            }
        }
    }*/

}
