package com.andremacareno.tcontestproj.views.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tcontestproj.AndroidUtilities;

/**
 * Created by andremacareno on 04/05/16.
 * WOW
 * SUCH ANIMATIONS
 * MUCH CODE
 * SO SOPHISTICATED
 * Tip: if you want to understand this code, just read methods in the end, such as inflateCircle()/deflateCircle()/etc. It's very sophisticated view :)
 */
//TODO reduce new() calls
public class CaptureButton extends View implements CameraControl {
    private static final String TAG = "Capture";
    private static final float ELASTIC_FACTOR = 0.5f;
    private int outerRadius, middleRadius, innerRadius, recRadius, recRecordingRadius;
    private int outerRectWidth, middleRectWidth;
    private Paint outerPaint, middlePaint, innerPaint, recPaint, recordingCirclePaint;

    private int centerX = -1, centerY = -1;
    private static final int RECT_HIDE_DURATION = 80;
    private static final int CIRC_INFLATE_DURATION = 322;
    private static final int DEFLATE_TOTAL_DURATION = 322;
    private static final int REC_ICON_INFLATE_DURATION = 180;
    private static final int REC_ANIM_DURATION = 80;
    private static final int ANIM_DURATION = 112;
    private static final int SWITCH_ANIM_DURATION = 180;
    private static final int LONGTAP_DURATION = 200;
    private static final int FADE_DURATION = 200;
    private static final int DELAY = 16;
    private int time, switchTime, recAnimTime, hideRectTime, inflateCircTime, inflateRecIconTime, deflateCircTime, fadeTime, longtapTime;
    private boolean processingCapture = false;
    private boolean captureAnimating = false;
    private boolean switchAnimating = false;
    private boolean recPointAnimating = false;
    //private boolean recIconAnimating = false;
    private boolean onRecordAnim = false;
    private boolean circAnimating = false;
    private boolean circDeflateAnimating = false;
    private boolean animateStopRecording = false;
    private boolean fadeAnimating = false;
    private boolean longtapAnimating = false;
    private float captureK = 0.0f, switchK = 0.0f, recK = 0.0f, hideRectK = 0.0f, inflateCircK = 0.0f, inflateRecIconK = 0.0f, deflateCircK = 0.0f, alphaK = 0.0f, longtapInflateRectK = 0.0f, longtapInflateCircleK = 0.0f;
    private final RectF outerCircle = new RectF();
    private final RectF middleCircle = new RectF();
    private final RectF stopRecIcon = new RectF();
    private boolean videoMode = false;
    private boolean longtapRecording = false;
    private boolean reverseLongtapAnim = false;
    static Handler longtapCoordinator = new Handler();
    private int rectDiff, middleRectDiff, outRectDiffOnRecord, recInflateDiff;
    private boolean reverse = false;

    private boolean recording = false;
    private CameraControlLayout.CameraController controller;
    private ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            if(Build.VERSION.SDK_INT >= 16)
                getViewTreeObserver().removeOnGlobalLayoutListener(this);
            else
                getViewTreeObserver().removeGlobalOnLayoutListener(this);
            centerX = getMeasuredWidth() / 2;
            centerY = getMeasuredHeight() / 2;
            if(!videoMode)
            {
                outerCircle.left = getOffsetWidth();
                outerCircle.right = getMeasuredWidth() - getOffsetWidth();
                outerCircle.top = getOffsetHeight();
                outerCircle.bottom = getMeasuredHeight() - getOffsetHeight();

                middleCircle.left = middleCircle.top = (outerRadius - middleRadius) / 2;
                middleCircle.left += getOffsetWidth();
                middleCircle.top += getOffsetHeight();
                middleCircle.right = getMeasuredWidth() - ((outerRadius - middleRadius) / 2) - getOffsetWidth();
                middleCircle.bottom = getMeasuredHeight() - ((outerRadius - middleRadius) / 2) - getOffsetHeight();
                postInvalidate();
            }
        }
    };

    private final Runnable captureAnimRunnable = new Runnable() {
        @Override
        public void run() {
            captureK = getProgress(time, 0, 1, ANIM_DURATION);
            if(captureK > 1.0f)
                captureK = 1.0f;
            time += DELAY;
            postInvalidate();
            if(time <= ANIM_DURATION)
                postDelayed(this, DELAY);
            else {
                time = 0;
                captureAnimating = false;
                if(controller != null)
                    controller.didPhotoSaveAllowed();
            }
        }
    };
    private final Runnable extendRunnable = new Runnable() {
        @Override
        public void run() {
            switchK = reverse ? getProgress(switchTime, 1, -1, SWITCH_ANIM_DURATION) : getProgress(switchTime, 0, 1, SWITCH_ANIM_DURATION);
            if(switchK > 1.0f)
                switchK = 1.0f;
            else if(switchK < 0.0f)
                switchK = 0.0f;
            switchTime += DELAY;
            if(switchTime <= SWITCH_ANIM_DURATION) {
                postInvalidate();
                postDelayed(this, DELAY);
            }
            else {
                switchAnimating = false;
                if(reverse) {
                    videoMode = false;
                    reverse = false;
                    switchK = 0.0f;
                }
                else
                    beginRecAnimation();
            }
        }
    };

    private final Runnable recAnimRunnable = new Runnable() {
        @Override
        public void run() {
            recK = reverse ? getProgress(recAnimTime, 1, -1, REC_ANIM_DURATION) : getProgress(recAnimTime, 0, 1, REC_ANIM_DURATION);
            if(recK > 1.0f)
                recK = 1.0f;
            else if(recK < 0.0f)
                recK = 0.0f;
            recAnimTime += DELAY;
            if(recAnimTime <= REC_ANIM_DURATION) {
                postInvalidate();
                postDelayed(this, DELAY);
            }
            else {
                recPointAnimating = false;
                if(reverse) {
                    extendAnimation();
                }
                else
                    reverse = true;
            }
        }
    };

    private final Runnable hideRectRunnable = new Runnable() {
        @Override
        public void run() {
            hideRectK = getProgress(hideRectTime, 0, 1, RECT_HIDE_DURATION);
            if(hideRectK > 1.0f)
                hideRectK = 1.0f;
            hideRectTime += DELAY;
            postInvalidate();
            if(hideRectTime <= RECT_HIDE_DURATION) {
                if(hideRectTime >= RECT_HIDE_DURATION * 0.3f) {
                    if(!circAnimating)
                        inflateCircK = 0.0f;
                    inflateCircle();
                }
                postDelayed(this, DELAY);
            }
            else
                onRecordAnim = false;
        }
    };


    private final Runnable inflateCircRunnable = new Runnable() {
        @Override
        public void run() {
            inflateCircK = easeOutElastic(inflateCircTime, 0, 1, CIRC_INFLATE_DURATION);
            inflateCircTime += DELAY;
            if(inflateCircTime >= CIRC_INFLATE_DURATION - REC_ICON_INFLATE_DURATION - 40)
            {
                inflateRecIconK = getProgress(inflateRecIconTime, 0, 1, REC_ICON_INFLATE_DURATION);
                if(inflateRecIconK >= 1.0f)
                    inflateRecIconK = 1.0f;
                inflateRecIconTime += DELAY;
            }
            else
                inflateRecIconK = 0.0f;
            if(inflateCircTime <= CIRC_INFLATE_DURATION) {
                postInvalidate();
                postDelayed(this, DELAY);
            }
            else {
                circAnimating = false;
            }
        }
    };

    private final Runnable deflateCircRunnable = new Runnable() {
        @Override
        public void run() {
            deflateCircK = getProgress(deflateCircTime, 0, 1, DEFLATE_TOTAL_DURATION);
            deflateCircTime += DELAY;
            if(deflateCircTime <= DEFLATE_TOTAL_DURATION) {
                postInvalidate();
                postDelayed(this, DELAY);
            }
            else {
                circDeflateAnimating = false;
                animateStopRecording = false;
                postInvalidate();
                if(controller != null)
                    controller.didVideoSaveAllowed();
                //getHandler().removeCallbacksAndMessages(null);
            }
        }
    };

    private final Runnable fadeOutRunnable = new Runnable() {
        @Override
        public void run() {
            alphaK = getProgress(fadeTime, 0, 1, FADE_DURATION);
            if(alphaK > 1.0f)
                alphaK = 1.0f;
            fadeTime += DELAY;
            if(fadeTime <= FADE_DURATION) {
                postInvalidate();
                postDelayed(this, DELAY);
            }
            else {
                fadeAnimating = false;
                setVisibility(GONE);
            }
        }
    };

    private final Runnable fadeInRunnable = new Runnable() {
        @Override
        public void run() {
            alphaK = getProgress(fadeTime, 1, -1, FADE_DURATION);
            if(alphaK > 1.0f)
                alphaK = 1.0f;
            fadeTime += DELAY;
            if(fadeTime <= FADE_DURATION) {
                postInvalidate();
                postDelayed(this, DELAY);
            }
            else {
                fadeAnimating = false;
            }
        }
    };
    private final Runnable longtapAnimRunnable = new Runnable() {
        @Override
        public void run() {
            if(!longtapAnimating)
                return;
            if(reverseLongtapAnim && longtapTime >= 80)
                longtapInflateRectK = getProgress(longtapTime - 80, 1, -1, LONGTAP_DURATION - 80);
            else if(!reverseLongtapAnim)
                longtapInflateRectK = getProgress(longtapTime, 0, 1, LONGTAP_DURATION - 80);
            longtapInflateCircleK = reverseLongtapAnim ? getLongTapCircleRadius(longtapTime, 1, -1, LONGTAP_DURATION, true) : getLongTapCircleRadius(longtapTime, 0, 1, LONGTAP_DURATION, false);
            if(longtapInflateRectK > 1.0f)
                longtapInflateRectK = 1.0f;
            else if(longtapInflateCircleK > 1.0f)
                longtapInflateCircleK = 1.0f;
            else if(longtapInflateCircleK < 0.0f)
                longtapInflateCircleK = 0.0f;
            else if(longtapInflateRectK < 0.0f)
                longtapInflateRectK = 0.0f;
            longtapTime += DELAY;
            if(longtapTime <= LONGTAP_DURATION) {
                postInvalidate();
                longtapCoordinator.postDelayed(this, DELAY);
            }
            else {
                longtapAnimating = false;
                if(reverseLongtapAnim)
                {
                    longtapRecording = false;
                    reverseLongtapAnim = false;
                    postInvalidate();
                    if(controller != null)
                        controller.didVideoSaveAllowed();
                }
            }
        }
    };

    public CaptureButton(Context context) {
        super(context);
        init();
    }

    public CaptureButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private int getOffsetWidth()
    {
        return (getMeasuredWidth() - outerRadius * 2) / 2;
    }
    private int getOffsetHeight()
    {
        return (getMeasuredHeight() - outerRadius*2) / 2;
    }
    private void init()
    {
        recInflateDiff = AndroidUtilities.dp(6);
        recRecordingRadius = AndroidUtilities.dp(35);
        outRectDiffOnRecord = AndroidUtilities.dp(70);
        rectDiff = AndroidUtilities.dp(8);
        recRadius = middleRectDiff = AndroidUtilities.dp(8);
        middleRectWidth = AndroidUtilities.dp(80);
        outerRectWidth = AndroidUtilities.dp(86);
        outerRadius = AndroidUtilities.dp(35);
        middleRadius = AndroidUtilities.dp(32);
        innerRadius = AndroidUtilities.dp(20);
        outerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        middlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        innerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        recPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        recordingCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        recordingCirclePaint.setColor(0xFFCD4747);
        outerPaint.setStyle(Paint.Style.FILL);
        middlePaint.setStyle(Paint.Style.FILL);
        innerPaint.setStyle(Paint.Style.FILL);
        recPaint.setStyle(Paint.Style.FILL);
        recordingCirclePaint.setStyle(Paint.Style.FILL);
        /*postDelayed(new Runnable() {
            @Override
            public void run() {
                switchToVideoMode();
            }
        }, 1000);*/

        getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
    }
    @Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        setMeasuredDimension(outerRectWidth, outerRadius * 2 + AndroidUtilities.dp(8));
    }

    @Override
    public void onDraw(Canvas canvas)
    {
        if(centerX <= 0 || centerY <= 0)
            return;
        canvas.drawARGB(0, 0, 0, 0);
        if(longtapRecording)
            drawLongTapTransitions(canvas);
        else if(recording)
            drawRecordDirectTransitions(canvas);
        else if(animateStopRecording)
            drawStopRecTransitions(canvas);
        else
            drawSwitchTransitions(canvas);
    }
    private void drawLongTapTransitions(Canvas canvas)
    {
        outerCircle.left = (outerRectWidth / 2 - outerRadius) + outerRadius*2*longtapInflateRectK;
        outerCircle.right = (outerRectWidth / 2 + outerRadius) - outerRadius*2*longtapInflateRectK;
        outerCircle.top = getOffsetHeight() + outerRadius*2*longtapInflateRectK;
        outerCircle.bottom = getMeasuredHeight() - getOffsetHeight() - outerRadius*2*longtapInflateRectK;

        middleCircle.left = middleCircle.top = (outerRectWidth / 2 - middleRadius) + middleRadius*2*longtapInflateRectK;
        middleCircle.right = middleCircle.bottom = (outerRectWidth / 2 + middleRadius) - middleRadius*2*longtapInflateRectK;

        middlePaint.setColor(0xFF3D96E3);

        stopRecIcon.left = centerX - recRadius - recInflateDiff * longtapInflateRectK;
        stopRecIcon.right = centerX + recRadius + recInflateDiff * longtapInflateRectK;
        stopRecIcon.top = centerY - recRadius - recInflateDiff * longtapInflateRectK;
        stopRecIcon.bottom = centerY + recRadius + recInflateDiff * longtapInflateRectK;
        canvas.drawRoundRect(outerCircle, outerRadius, outerRadius, outerPaint);
        canvas.drawRoundRect(middleCircle, middleRadius, middleRadius, outerPaint);
        canvas.drawCircle(centerX, centerY, innerRadius * (1.0f - longtapInflateRectK), innerPaint);

        //if(longtapInflateCircleK > 0.0f)
        canvas.drawCircle(centerX, centerY, recRadius + (recRecordingRadius - recRadius) * longtapInflateCircleK, recordingCirclePaint);
        canvas.drawRoundRect(stopRecIcon, recRadius, recRadius, outerPaint);
    }
    private void drawStopRecTransitions(Canvas canvas)
    {
        outerCircle.left = outerRectWidth / 2 - (outerRadius + rectDiff) * deflateCircK;
        outerCircle.right = outerRectWidth / 2 + (outerRadius + rectDiff) * deflateCircK;
        outerCircle.top = getOffsetHeight() + getMeasuredHeight()*0.88f * (1.0f - deflateCircK);
        outerCircle.bottom = getMeasuredHeight() - getOffsetHeight() - getMeasuredHeight()*0.88f * (1.0f - deflateCircK);
        canvas.drawRoundRect(outerCircle, outerRadius, outerRadius, outerPaint);
        canvas.drawCircle(centerX, centerY, recRadius + (recRecordingRadius - recRadius) * (1.0f - deflateCircK), recordingCirclePaint);
    }
    private void drawRecordDirectTransitions(Canvas canvas)
    {
        if(onRecordAnim)
        {
            outerCircle.left = outerRectWidth / 2 - outerRadius + outRectDiffOnRecord * hideRectK + getOffsetWidth()*2;
            outerCircle.right = outerRectWidth / 2 + outerRadius - outRectDiffOnRecord * hideRectK - getOffsetWidth()*2;
        }
        /*else
        {
            outerCircle.left = outerRectWidth / 2 - outerRadius + outRectDiffOnRecord + getOffsetWidth() * 2;
            outerCircle.right = outerRectWidth / 2 + outerRadius - outRectDiffOnRecord - getOffsetWidth()*2;
        }*/
        if(onRecordAnim)
            canvas.drawRoundRect(outerCircle, outerRadius, outerRadius, outerPaint);
        canvas.drawCircle(centerX, centerY, recRadius + (recRecordingRadius - recRadius) * inflateCircK, recordingCirclePaint);
        //if(recIconAnimating)
        //{
            stopRecIcon.left = centerX - recRadius - recInflateDiff * inflateRecIconK;
            stopRecIcon.right = centerX + recRadius + recInflateDiff * inflateRecIconK;
            stopRecIcon.top = centerY - recRadius - recInflateDiff * inflateRecIconK;
            stopRecIcon.bottom = centerY + recRadius + recInflateDiff * inflateRecIconK;
        //}
        /*else if(!circAnimating)
        {
            stopRecIcon.left = centerX - recRadius;
            stopRecIcon.right = centerX + recRadius;
            stopRecIcon.top = centerY - recRadius;
            stopRecIcon.bottom = centerY + recRadius;
        }*/
        canvas.drawRoundRect(stopRecIcon, recRadius, recRadius, outerPaint);
    }
    private void drawSwitchTransitions(Canvas canvas)
    {
        outerPaint.setColor(Color.argb((int) (0xFF * (1.0f - alphaK)), 0xFF, 0xFF, 0xFF));
        middlePaint.setColor(Color.argb((int) (0xFF * (1.0f - alphaK)), 0x3D, 0x96, 0xE3));
        innerPaint.setColor(Color.argb((int) (0xFF * (1.0f - alphaK)), 0x50, 0xA8, 0xF5));
        if(switchAnimating && !recPointAnimating)
        {
            outerCircle.left = outerRectWidth / 2 - outerRadius - rectDiff * switchK;
            outerCircle.right = outerRectWidth / 2 + outerRadius + rectDiff * switchK;
            middleCircle.left = outerRectWidth / 2 - middleRadius - middleRectDiff * switchK;
            middleCircle.right = outerRectWidth / 2 + middleRadius + middleRectDiff * switchK;
            middlePaint.setColor(Color.argb((int) (0xFF * (1.0 - switchK)), 0x3D, 0x96, 0xE3));
        }
        /*else if(videoMode)
        {
            outerCircle.left = 0;
            outerCircle.right = getMeasuredWidth();
            middlePaint.setColor(0xFF3D96E3);
        }*/
        /*
        outerPaint.setColor(0xFFFFFFFF);
        middlePaint.setColor(0xFF3D96E3);
        innerPaint.setColor(0xFF50A8F5);
         */
        canvas.drawRoundRect(outerCircle, outerRadius, outerRadius, outerPaint);
        if(videoMode && !isAnimating())
        {
            recPaint.setColor(0xFFDF2E38);
            canvas.drawCircle(centerX, centerY, recRadius * recK, recPaint);
            return;
        }
        else if(videoMode && fadeAnimating)
        {
            recPaint.setColor(Color.argb((int) (0xFF * (1.0f - alphaK)), 0xDF, 0x2E, 0x38));
            canvas.drawCircle(centerX, centerY, recRadius * recK, recPaint);
            return;
        }
        else
            recPaint.setColor(0xFFDF2E38);
        if(!processingCapture && !recPointAnimating) {
            if(videoMode && !switchAnimating)
            {
                middleCircle.left = (outerRectWidth - middleRectWidth) / 2 + getOffsetWidth();
                middleCircle.right = getMeasuredWidth() - (outerRectWidth - middleRectWidth) / 2 - getOffsetWidth();
            }
            else if(!switchAnimating)
            {
                middleCircle.left = captureAnimating ? getMeasuredWidth() / 2 - middleRadius * captureK : getMeasuredWidth() / 2 - middleRadius;
                middleCircle.top  = captureAnimating ? getMeasuredHeight() / 2 - middleRadius * captureK : getMeasuredHeight() / 2 - middleRadius;
                middleCircle.right = captureAnimating ? getMeasuredWidth() / 2 + middleRadius * captureK : getMeasuredWidth() / 2 + middleRadius;
                middleCircle.bottom = captureAnimating ? getMeasuredHeight() / 2 + middleRadius * captureK : getMeasuredHeight() / 2 + middleRadius;
            }
            canvas.drawRoundRect(middleCircle, middleRadius, middleRadius, middlePaint);
        }
        if(recPointAnimating) {
            recPaint.setColor(Color.argb((int) (0xFF * recK), 0xDF, 0x2E, 0x38));
            canvas.drawCircle(centerX, centerY, recRadius * recK, recPaint);
        }
        else
            canvas.drawCircle(centerX, centerY, switchAnimating ? (int) (innerRadius * (1.0 - switchK)) : innerRadius, innerPaint);
    }
    public boolean onCapture()
    {
        if(isAnimating())
            return false;
        processingCapture = true;
        postInvalidate();
        return true;
    }
    public boolean onCaptureFinish()
    {
        if(isAnimating())
            return false;
        processingCapture = false;
        beginCaptureAnimation();
        return true;
    }
    public boolean switchToVideoMode()
    {
        if(isAnimating())
            return false;
        videoMode = true;
        beginSwitchAnimation();
        return true;
    }
    public boolean switchToCaptureMode()
    {
        if(isAnimating())
            return false;
        beginSwitchAnimation();
        return true;
    }

    public boolean didRecordStarted()
    {
        if(isAnimating())
            return false;
        recording = true;
        hideRect();
        return true;
    }
    public boolean didRecordStopped()
    {
        if(isAnimating())
            return false;
        recording = false;
        animateStopRecording = true;
        deflateCircle();
        return true;
    }
    public boolean hide()
    {
        if(isAnimating()) {
            setVisibility(GONE);
            return false;
        }
        performFadeOut();
        return true;
    }
    public boolean show()
    {
        setVisibility(VISIBLE);
        if(isAnimating())
            return false;
        performFadeIn();
        return true;
    }
    public boolean didLongtapRecStarted()
    {
        if(isAnimating())
            return false;
        longtapRecording = true;
        longtapAnim();
        return true;
    }
    public boolean didLongtapRecordStopped()
    {
        if(longtapAnimating) {
            longtapAnimating = false;
            reverseLongtapAnim = false;
            longtapRecording = false;
            longtapCoordinator.removeCallbacksAndMessages(null);
            outerCircle.left = (outerRectWidth / 2 - outerRadius);
            outerCircle.right = (outerRectWidth / 2 + outerRadius);
            outerCircle.top = getOffsetHeight();
            outerCircle.bottom = getMeasuredHeight() - getOffsetHeight();

            middleCircle.left = middleCircle.top = (outerRectWidth / 2 - middleRadius);
            middleCircle.right = middleCircle.bottom = (outerRectWidth / 2 + middleRadius);

            middlePaint.setColor(0xFF3D96E3);
            postInvalidate();
            return true;
        }
        else if(isAnimating())
            return false;
        reverseLongtapAnim = true;
        longtapAnim();
        return true;
    }

    private void beginCaptureAnimation()
    {
        if(captureAnimating)
            return;
        captureAnimating = true;
        time = 0;
        captureK = 0;
        postDelayed(captureAnimRunnable, DELAY);
    }
    private void beginSwitchAnimation()
    {
        if(reverse)
            beginRecAnimation();
        else
            extendAnimation();
    }
    private void extendAnimation()
    {
        switchAnimating = true;
        switchTime = 0;
        postDelayed(extendRunnable, DELAY);
    }
    private void beginRecAnimation()
    {
        recPointAnimating = true;
        recAnimTime = 0;
        postDelayed(recAnimRunnable, DELAY);
    }

    //on record tap
    private void hideRect()
    {
        if(onRecordAnim)
            return;
        onRecordAnim = true;
        hideRectTime = 0;
        inflateCircTime = 0;
        inflateRecIconTime = 0;
        hideRectK = 0.0f;
        postDelayed(hideRectRunnable, DELAY);
    }
    private void inflateCircle()
    {
        if(circAnimating)
            return;
        circAnimating = true;
        inflateCircTime = 0;
        postDelayed(inflateCircRunnable, DELAY);
    }
    /*private void inflateRecIcon()
    {
        if(recIconAnimating)
            return;
        recIconAnimating = true;
        inflateRecIconTime = 0;
        postDelayed(new Runnable() {
            @Override
            public void run() {
                inflateRecIconK = getProgress(inflateRecIconTime, 0, 1, REC_ICON_INFLATE_DURATION);
                if(inflateRecIconK >= 1.0f)
                    inflateRecIconK = 1.0f;
                inflateRecIconTime += DELAY;
                if(inflateRecIconTime <= REC_ICON_INFLATE_DURATION) {
                    if(!circAnimating)
                        postInvalidate();
                    postDelayed(this, DELAY);
                }
            }
        }, DELAY);
    }*/
    private void deflateCircle()
    {
        if(circDeflateAnimating)
            return;
        circDeflateAnimating = true;
        deflateCircTime = 0;
        postDelayed(deflateCircRunnable, DELAY);
    }
    private void performFadeOut()
    {
        if(fadeAnimating)
            return;
        fadeAnimating = true;
        fadeTime = 0;
        postDelayed(fadeOutRunnable, DELAY);
    }
    private void performFadeIn()
    {
        if(fadeAnimating)
            return;
        fadeAnimating = true;
        fadeTime = 0;
        postDelayed(fadeInRunnable, DELAY);
    }

    //probably best example of how to do complex animation instead of creating two methods
    private void longtapAnim()
    {
        longtapAnimating = true;
        longtapTime = 0;
        longtapCoordinator.postDelayed(longtapAnimRunnable, DELAY);
    }
    public boolean isAnimating()
    {
        return longtapAnimating || fadeAnimating || captureAnimating || switchAnimating || recPointAnimating || onRecordAnim || circAnimating || animateStopRecording || circDeflateAnimating;
    }

    private static float getProgress(float time, float start, float delta, float duration)
    {
        time /= duration;
        time--;
        return -delta * (time * time * time * time - 1) + start;
    }
    private static float easeOutElastic(float time, float start, float delta, float duration)
    {
        time /= duration;
        float t = (float) (Math.pow(2,-10*time) * Math.sin((time-ELASTIC_FACTOR/4)*(2*Math.PI)/ELASTIC_FACTOR)) + 1.0f;
        return start + t*delta;
    }

    private static float getLongTapCircleRadius(float time, float start, float delta, float duration, boolean reverse)
    {
        if(!reverse && time <= 80)
            return 0;
        else if(reverse && time >= duration - 80)
            return 0;
        if(!reverse)
            time -= 80;
        time /= duration;
        time--;
        return -delta * (time * time * time * time - 1) + start;
    }

    @Override
    public void setController(CameraControlLayout.CameraController controller) {
        this.controller = controller;
    }
}
