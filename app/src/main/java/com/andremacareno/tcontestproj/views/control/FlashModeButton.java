package com.andremacareno.tcontestproj.views.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.R;

/**
 * Created by andremacareno on 04/05/16.
 */
public class FlashModeButton extends View implements CameraControl {
    private Drawable autoFlash, flashOn, flashOff;
    private static final int ANIM_DURATION = 300;
    private static final int DELAY = 16;
    private int time;

    private float k = 0.0f;
    boolean animating = false;
    private int paddingTop;
    private FlashMode mode = FlashMode.AUTO;
    private FlashMode previousMode = null;
    private CameraControlLayout.CameraController controller;

    private Runnable translateRunnable = new Runnable() {
        @Override
        public void run() {
            k = getProgress(time, 0, 1, ANIM_DURATION);
            if(k > 1.0f)
                k = 1.0f;
            time += DELAY;
            postInvalidate();
            if(time < ANIM_DURATION)
                postDelayed(this, DELAY);
            else {
                animating = false;
            }
        }
    };

    public FlashModeButton(Context context) {
        super(context);
        init();
    }

    public FlashModeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        paddingTop = AndroidUtilities.dp(16);
        flashOn = ContextCompat.getDrawable(getContext(), R.drawable.flash_on);
        flashOff = ContextCompat.getDrawable(getContext(), R.drawable.flash_off);
        autoFlash = ContextCompat.getDrawable(getContext(), R.drawable.flash_auto);
        flashOff.setBounds(0, paddingTop, flashOff.getIntrinsicWidth(), flashOff.getIntrinsicHeight()+paddingTop);
        flashOn.setBounds(0, paddingTop, flashOn.getIntrinsicWidth(), flashOn.getIntrinsicHeight()+paddingTop);
        autoFlash.setBounds(0, paddingTop, autoFlash.getIntrinsicWidth(), autoFlash.getIntrinsicHeight()+paddingTop);
        ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(Build.VERSION.SDK_INT >= 16)
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                else
                    getViewTreeObserver().removeGlobalOnLayoutListener(this);
                postInvalidate();
            }
        };
        getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                FlashMode next = nextMode();
                if(controller != null && next != null)
                    controller.didFlashModeChangeRequested(next);
            }
        });
    }
    @Override
    public void onMeasure(int measureWidthSpec, int measureHeightSpec)
    {
        if(flashOn != null)
            setMeasuredDimension(flashOn.getIntrinsicWidth(), flashOn.getIntrinsicHeight()*3+paddingTop);
    }
    public FlashMode nextMode()
    {
        if(animating)
            return null;
        animating = true;
        previousMode = mode;
        if(mode == FlashMode.AUTO)
            mode = FlashMode.ON;
        else if(mode == FlashMode.ON)
            mode = FlashMode.OFF;
        else
            mode = FlashMode.AUTO;
        performTranslate();
        return mode;
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        if(getMeasuredWidth() <= 0)
            return;
        canvas.drawARGB(0, 0, 0, 0);
        Drawable prevDrawable = getDrawableByMode(previousMode);
        Drawable nextDrawable = getDrawableByMode(mode);
        nextDrawable.setAlpha(255);
        if(prevDrawable != null && animating)
        {
            prevDrawable.setAlpha((int) (255 * (1.0f - k)));
            prevDrawable.setBounds(0, (int) (paddingTop + 2 * prevDrawable.getIntrinsicHeight() * k), prevDrawable.getIntrinsicWidth(), (int) (paddingTop + prevDrawable.getIntrinsicHeight() * (1 + 2 * k)));
            prevDrawable.draw(canvas);
        }
        if(animating && nextDrawable != null)
        {
            nextDrawable.setBounds(0, (int) (-nextDrawable.getIntrinsicHeight() + (nextDrawable.getIntrinsicHeight() + paddingTop) * k), nextDrawable.getIntrinsicWidth(), (int) ((paddingTop + nextDrawable.getIntrinsicHeight()) * k) );
            nextDrawable.draw(canvas);
        }
        else if(!animating)
        {
            nextDrawable.setBounds(0, paddingTop, nextDrawable.getIntrinsicWidth(), nextDrawable.getIntrinsicHeight()+paddingTop);
            nextDrawable.draw(canvas);
        }
    }

    private Drawable getDrawableByMode(FlashMode m)
    {
        if(m == FlashMode.AUTO)
            return autoFlash;
        else if(m == FlashMode.ON)
            return flashOn;
        else if(m == FlashMode.OFF)
            return flashOff;
        return null;
    }

    public void performTranslate()
    {
        time = 0;
        postDelayed(translateRunnable, DELAY);
    }

    //easeOutQuart
    private static float getProgress(float time, float start, float delta, float duration)
    {
        time /= duration;
        time--;
        return -delta * (time * time * time * time - 1) + start;
    }
    public boolean isAnimating()
    {
        return animating;
    }
    @Override
    public void setController(CameraControlLayout.CameraController controller) {
        this.controller = controller;
    }
    public void onFlashInfo(boolean hasFlash)
    {
        setVisibility(hasFlash ? VISIBLE : GONE);

    }
}
