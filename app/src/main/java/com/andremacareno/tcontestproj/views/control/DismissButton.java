package com.andremacareno.tcontestproj.views.control;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.andremacareno.tcontestproj.R;

/**
 * Created by andremacareno on 06/05/16.
 */
public class DismissButton extends View implements View.OnTouchListener {
    private final static float ELASTIC_FACTOR = 0.4f;
    private final int DELAY = 16;
    private final int ANIM_DURATION = 550;
    private Drawable dismissDrawable, dismissDrawablePressed;
    private int centerX, centerY;
    private boolean animatingAlpha;
    private int alphaTime;
    private float k = 0.0f;
    boolean pressed = false;

    private final Runnable showRunnable = new Runnable() {
        @Override
        public void run() {
            k = getProgress(alphaTime, 0, 1, ANIM_DURATION);
            alphaTime += DELAY;
            postInvalidate();
            if(alphaTime < ANIM_DURATION)
                postDelayed(this, DELAY);
            else {
                animatingAlpha = false;
            }
        }
    };

    public DismissButton(Context context) {
        super(context);
        init();
    }

    public DismissButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init()
    {
        dismissDrawable = ContextCompat.getDrawable(getContext(), R.drawable.btn_cancel);
        dismissDrawablePressed = ContextCompat.getDrawable(getContext(), R.drawable.btn_cancel_pressed);
        setOnTouchListener(this);
    }

    @Override
    public void onMeasure(int widthSpec, int heightSpec)
    {
        setMeasuredDimension((int) (dismissDrawable.getIntrinsicWidth() * 1.25f), (int) (dismissDrawable.getIntrinsicHeight() * 1.25f));
    }
    @Override
    public boolean onTouch(View view, MotionEvent e)
    {
        if(e.getAction() == MotionEvent.ACTION_DOWN) {
            pressed = true;
            postInvalidate();
        }
        else if(e.getAction() == MotionEvent.ACTION_CANCEL || e.getAction() == MotionEvent.ACTION_UP)
        {
            pressed = false;
            postInvalidate();
        }
        return false;
    }
    @Override
    public void onDraw(Canvas canvas)
    {
        if(getMeasuredWidth() == 0 || getMeasuredHeight() == 0)
            return;
        if(centerX <= 0 || centerY <= 0) {
            centerX = getMeasuredWidth() / 2;
            centerY = getMeasuredHeight() / 2;
            if(centerX <= 0 || centerY <= 0)
                return;
            int padding = (getMeasuredWidth() - dismissDrawable.getIntrinsicWidth()) / 2;
            dismissDrawable.setBounds(padding, padding, getMeasuredWidth() - padding, getMeasuredHeight() - padding);
            dismissDrawablePressed.setBounds(padding, padding, getMeasuredWidth() - padding, getMeasuredHeight() - padding);
        }
        canvas.drawARGB(0, 0, 0, 0);
        canvas.scale(k, k, centerX, centerY);
        if(pressed)
            dismissDrawablePressed.draw(canvas);
        else
            dismissDrawable.draw(canvas);
    }
    public void show()
    {
        if(animatingAlpha)
            return;
        animatingAlpha = true;
        alphaTime = 0;
        setVisibility(VISIBLE);
        postDelayed(showRunnable, DELAY);
    }
    public void hide()
    {
        k = 0.0f;
        setVisibility(GONE);
    }
    public boolean isAnimating()
    {
        return animatingAlpha;
    }
    private static float getProgress(float time, float start, float delta, float duration)
    {
        time /= duration;
        float t = (float) (Math.pow(2,-10*time) * Math.sin((time-ELASTIC_FACTOR/4)*(2*Math.PI)/ELASTIC_FACTOR)) + 1.0f;
        return start + t*delta;
    }
}
