package com.andremacareno.tcontestproj;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Handler;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.io.File;


/**
 * Created by andremacareno on 03/04/15.
 */
public class AppLoader extends Application {
    private final String TAG = "AppLoader";
    private static volatile AppLoader sharedApp;

    private volatile SharedPreferences prefs;
    public static AppLoader sharedInstance() {
        return sharedApp;
    }
    public static volatile Handler applicationHandler = new Handler();
    private File cacheDir;
    private String pathToCache;
    private String docsCache;
    private String tmpCache;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        cacheDir = getApplicationContext().getExternalCacheDir() == null ? getApplicationContext().getCacheDir() : getApplicationContext().getExternalCacheDir();
        pathToCache = cacheDir.getPath();
        docsCache = pathToCache.concat("/docs/");
        tmpCache = pathToCache.concat("/tmp/");
        sharedApp = this;
    }

    public SharedPreferences sharedPreferences() {
        if (prefs == null) {
            synchronized (SharedPreferences.class) {
                if (prefs == null)
                    prefs = getSharedPreferences(Constants.APP_PREFS, MODE_PRIVATE);
            }
        }
        return prefs;
    }


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level >= TRIM_MEMORY_MODERATE) {
            /*ImageCache.sharedInstance().evictAll();
            FileCache.getInstance().clear();
            UserCache.getInstance().clear();
            GroupChatFullCache.getInstance().clear();*/
            //writeThatTDLibMightBeReloaded();
        } else if (level >= TRIM_MEMORY_BACKGROUND) {
            //ImageCache.sharedInstance().trimToSize(ImageCache.sharedInstance().size() / 2);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        /*ImageCache.sharedInstance().evictAll();
        FileCache.getInstance().clear();
        UserCache.getInstance().clear();
        GroupChatFullCache.getInstance().clear();*/
    }
    public File cacheLocation() { return this.cacheDir; }
    public String stringCacheLocation() { return this.pathToCache; }
    public String docsCacheLocation() { return this.docsCache; }
    public String tmpCacheLocation() { return this.tmpCache; }
}
