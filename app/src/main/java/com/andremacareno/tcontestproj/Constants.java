package com.andremacareno.tcontestproj;
/**
 * Created by Andrew on 07.01.2016.
 */
public class Constants {
    public static final String APP_PREFS = "app_prefs";
    public static final String CAMERA_FRAGMENT = "camera_fragment";
    private static int totalPermissions = 0;
    public static final int REQUEST_CAMERA_PERMISSION = totalPermissions++;
    public static final int REQUEST_STORAGE_PERMISSION = totalPermissions++;
}
