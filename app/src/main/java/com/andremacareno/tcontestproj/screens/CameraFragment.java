package com.andremacareno.tcontestproj.screens;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.andremacareno.asynccore.notificationcenter.Notification;
import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.asynccore.notificationcenter.NotificationObserver;
import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.AppLoader;
import com.andremacareno.tcontestproj.BackPressListener;
import com.andremacareno.tcontestproj.BuildConfig;
import com.andremacareno.tcontestproj.MainActivity;
import com.andremacareno.tcontestproj.R;
import com.andremacareno.tcontestproj.compat.StillCaptureHelper;
import com.andremacareno.tcontestproj.compat.VideoCaptureHelper;
import com.andremacareno.tcontestproj.utils.FastBitmapDrawable;
import com.andremacareno.tcontestproj.views.AutoFitSurfaceView;
import com.andremacareno.tcontestproj.views.AutoFitTextureView;
import com.andremacareno.tcontestproj.views.EditorDelegate;
import com.andremacareno.tcontestproj.views.OnShootView;
import com.andremacareno.tcontestproj.views.control.CameraControlLayout;
import com.andremacareno.tcontestproj.views.control.CameraMode;
import com.andremacareno.tcontestproj.views.control.FlashMode;
import com.andremacareno.tcontestproj.views.control.FlashModeButton;
import com.andremacareno.tcontestproj.views.control.ModeSwitch;
import com.andremacareno.tcontestproj.views.control.RecordingTimer;

import java.io.File;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by andremacareno on 27/04/16.
 */
//TODO reduce camera controls memory consumption
public class CameraFragment extends Fragment implements CameraControlLayout.CameraController, EditorDelegate{
    public interface RotationCallback
    {
        void onRotate(float degrees);
    }
    boolean recording = false;
    boolean longtapRecording = false;
    volatile boolean processing = false;
    CameraMode mode = CameraMode.STILL;
    private int touchX = -1;
    FlashModeButton flash;
    ModeSwitch sw;
    CameraControlLayout controls;

    boolean saveVideoAllowed = false;
    boolean savePhotoAllowed = false;
    boolean editButtonTouched = false;
    boolean edited = false;

    private View finder;
    private OnShootView onShoot;
    private PhotoViewAttacher photoViewAttacher;
    private VideoView videoView;
    private FrameLayout videoContainer;
    private RecordingTimer timer;
    private boolean editMode = false;
    private boolean playingVideo = false;
    //private File tmpFile = null;
    private ProgressDialog savingPd;
    private final AtomicBoolean savingTmp = new AtomicBoolean(false);
    private final AtomicBoolean savingToGallery = new AtomicBoolean(false);
    private String pathToVideo = null;
    private Bitmap bmpRef = null;
    private Drawable bmpDrawable = null;
    private BitmapDrawable videoPreview = null;
    private boolean prepared = false;
    private volatile boolean hasFlash = false;
    private volatile boolean failed = false;
    private volatile int camerasCount = 0;
    private final BackPressListener decisionBackPress = new BackPressListener() {
        @Override
        public void onBackPressed() {
            try
            {
                if(editMode)
                {
                    didCancelRequested();
                    return;
                }
                if(bmpRef != null || pathToVideo != null)
                    controls.performDismissAction();
                ((MainActivity) getActivity()).removeBackPressInterceptor(this);
            }
            catch(Exception ignored) {}
        }
    };
    private final Runnable turnVideoOn = new Runnable() {
        @Override
        public void run() {
            VideoCaptureHelper.sharedInstance().onUiResume();
        }
    };
    private final Runnable turnStillCaptureOn = new Runnable() {
        @Override
        public void run() {
            StillCaptureHelper.sharedInstance().onUiResume();
        }
    };
    private Runnable restoreRunnable = new Runnable() {
        @Override
        public void run() {
            try
            {
                Toast.makeText(getContext(), R.string.too_fast, Toast.LENGTH_SHORT).show();
                sw.show();
                controls.swap.show();
                controls.showTransparency();
                controls.allowClickEvent();
            }
            catch(Exception ignored) {}
        }
    };
    private final MediaPlayer.OnCompletionListener videoFinish = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.reset();
            playingVideo = false;
            controls.actionButton.setImageResource(R.drawable.video_play);
            videoView.setVideoPath(pathToVideo);
            videoView.setMediaController(null);
            videoView.setOnCompletionListener(videoFinish);
            videoView.setOnErrorListener(videoError);
            videoView.setOnPreparedListener(mayStartVideo);
            /*if(videoPreview != null)
            {
                if(Build.VERSION.SDK_INT >= 16)
                    videoContainer.setBackground(videoPreview);
                else
                    videoContainer.setBackgroundDrawable(videoPreview);
            }*/
        }
    };
    private final MediaPlayer.OnErrorListener videoError = new MediaPlayer.OnErrorListener() {

        @Override
        public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
            mediaPlayer.reset();
            controls.actionButton.setImageResource(R.drawable.video_play);
            return true;
        }
    };
    private final MediaPlayer.OnPreparedListener mayStartVideo = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            prepared = true;
        }
    };
    private final View.OnClickListener playPauseClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(!prepared)
                return;
            videoView.setVisibility(View.VISIBLE);
            if(playingVideo)
            {
                playingVideo = false;
                controls.actionButton.setImageResource(R.drawable.video_play);
                videoView.pause();
            }
            else
            {
                playingVideo = true;
                controls.actionButton.setImageResource(R.drawable.video_pause);
                videoView.requestFocus();
                videoContainer.setBackgroundColor(0xFF333333);
                videoView.start();
            }
        }
    };
    /*private final View.OnClickListener discardClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            closeSaveScreen();
        }
    };
    private final View.OnClickListener saveClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try
            {
                NotificationCenter.getInstance().addObserver(storagePermissionGranted);
                ((MainActivity) getActivity()).requestStoragePermission();
            }
            catch(Exception ignored) {}
        }..........
    };*/
    private final NotificationObserver flashInfo = new NotificationObserver(NotificationCenter.didFlashInfoReceived) {
        @Override
        public void didNotificationReceived(final Notification notification) {
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    hasFlash = notification.getObject() == null ? false : (boolean) notification.getObject();
                    if(mode == CameraMode.STILL)
                        flash.onFlashInfo(hasFlash);
                }
            });
        }
    };
    private final NotificationObserver camCountObserver = new NotificationObserver(NotificationCenter.didCameraCountReceived) {
        @Override
        public void didNotificationReceived(final Notification notification) {
            camerasCount = (int) notification.getObject();
            if(BuildConfig.DEBUG)
                Log.d("CameraFragment", String.format("camerasCount = %d", camerasCount));
        }
    };
    private final NotificationObserver storagePermissionGranted = new NotificationObserver(NotificationCenter.didStoragePermissionGranted) {
        @Override
        public void didNotificationReceived(Notification notification) {
            if(BuildConfig.DEBUG)
                Log.d("CameraFragment", "permissions granted");
            NotificationCenter.getInstance().removeObserver(this);
            movePhotoToGallery();
        }
    };
    private final NotificationObserver onPictureTaken = new NotificationObserver(NotificationCenter.didPictureTaken) {
        @Override
        public void didNotificationReceived(final Notification notification) {
            try
            {
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bmp = (Bitmap) notification.getObject();
                        processing = false;
                        controls.capture.onCaptureFinish();
                        if(bmp == null) {
                            return;
                        }
                        bmpRef = bmp;
                        goToSavePhoto();
                    }
                });
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    };
    private final NotificationObserver videoRecFinished = new NotificationObserver(NotificationCenter.didVideoRecorded) {
        @Override
        public void didNotificationReceived(final Notification notification) {
            try
            {
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        pathToVideo = (String) notification.getObject();
                        if(pathToVideo == null)
                        {
                            failed = true;
                            processing = false;
                            restoreCamera();
                        }
                    }
                });
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    };
    private final NotificationObserver videoProcessed = new NotificationObserver(NotificationCenter.didVideoProcessed) {
        @Override
        public void didNotificationReceived(Notification notification) {
            try
            {
                //processing = false;
                Bitmap previewBmp = (Bitmap) notification.getObject();
                videoPreview = new BitmapDrawable(getResources(), previewBmp);
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(videoPreview != null)
                        {
                            processing = false;
                            Log.d("Fragment", "preview processed");
                            videoView.setVisibility(View.GONE);
                            if(Build.VERSION.SDK_INT >= 16)
                                videoContainer.setBackground(videoPreview);
                            else
                                videoContainer.setBackgroundDrawable(videoPreview);
                        }
                        goToSaveVideo();
                    }
                });
            }
            catch(Exception ignored) {}
        }
    };
    private final NotificationObserver onPictureTmpSaved = new NotificationObserver(NotificationCenter.didTakenPictureTemporarySaved) {
        @Override
        public void didNotificationReceived(Notification notification) {
            synchronized(savingTmp)
            {
                savingTmp.set(false);
            }
            //tmpFile = (File) notification.getObject();
        }
    };
    /*private final View.OnClickListener takePictureListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            synchronized(savingTmp)
            {
                if(savingTmp.get() && getCurrentCameraMode() != CameraMode.VIDEO)
                    return;
                savingTmp.set(true);
            }
            if(videoMode)
                VideoCaptureHelper.sharedInstance().toggleRecord();
            else
                StillCaptureHelper.sharedInstance().takePicture();
        }
    };
    private final View.OnClickListener swapCameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(videoMode)
                VideoCaptureHelper.sharedInstance().swapCamera();
            else
                StillCaptureHelper.sharedInstance().swapCamera();
        }
    };
    private final View.OnClickListener editClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onShoot.setEditMode(true);
            photoViewAttacher.scaleToNaturalSize();
        }
    };
    private final View.OnClickListener resetClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            photoViewAttacher.resetRotation();
        }
    };
    private final View.OnClickListener rotateClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            photoViewAttacher.rotateCcw();
        }
    };
    private final View.OnClickListener cancelClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            photoViewAttacher.resetRotation();
            onShoot.setEditMode(false);
        }
    };
    private final View.OnClickListener doneClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            onShoot.setEditMode(false);
        }
    };*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ((MainActivity)getActivity()).setKeepScreenOn(true);
        FrameLayout viewsContainer = new FrameLayout(container.getContext());
        videoContainer = new FrameLayout(container.getContext());
        FrameLayout.LayoutParams vidContainerLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        vidContainerLP.gravity = Gravity.CENTER;
        viewsContainer.setBackgroundColor(0xFF333333);
        FrameLayout.LayoutParams finderLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        finderLP.gravity = Gravity.CENTER_HORIZONTAL;
        videoView = new VideoView(getContext());
        videoView.setZOrderMediaOverlay(true);
        finder = Build.VERSION.SDK_INT >= 21 ? new AutoFitTextureView(container.getContext()) : new AutoFitSurfaceView(container.getContext());
        //finder = new SurfaceView(container.getContext());
        finder.setLayoutParams(finderLP);
        FrameLayout.LayoutParams fullScreenLP = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        finder.setLayoutParams(fullScreenLP);
        viewsContainer.setLayoutParams(fullScreenLP);
        videoContainer.setLayoutParams(fullScreenLP);
        videoView.setLayoutParams(vidContainerLP);
        StillCaptureHelper.sharedInstance().attachTextureView(finder);
        VideoCaptureHelper.sharedInstance().attachTextureView(finder);
        timer = new RecordingTimer(getContext());
        FrameLayout.LayoutParams timerLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(50));
        timerLp.gravity = Gravity.TOP;
        timer.setLayoutParams(timerLp);
        timer.setVisibility(View.GONE);
        onShoot = new OnShootView(container.getContext());
        onShoot.setEditorDelegate(this);
        onShoot.setLayoutParams(fullScreenLP);
        onShoot.setVisibility(View.GONE);
        videoContainer.setVisibility(View.GONE);
        videoView.setVisibility(View.GONE);

        View videoContainerBg = new View(getContext());
        videoContainerBg.setBackgroundColor(0xFF333333);
        videoContainerBg.setLayoutParams(fullScreenLP);
        videoContainer.addView(videoContainerBg);
        videoContainer.addView(videoView);
        viewsContainer.addView(finder);
        viewsContainer.addView(onShoot);
        viewsContainer.addView(videoContainer);
        photoViewAttacher = new PhotoViewAttacher(onShoot.imageView);
        photoViewAttacher.setDelegate(this);
        photoViewAttacher.setRotationCallback(new RotationCallback() {
            @Override
            public void onRotate(float degrees) {
                onShoot.cropView.rotate(degrees);
            }
        });
        savingPd = new ProgressDialog(getActivity());
        savingPd.setIndeterminate(true);
        savingPd.setCancelable(false);
        savingPd.setMessage(getResources().getString(R.string.saving));


        //controls init
        FrameLayout.LayoutParams flashLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        flashLp.rightMargin = AndroidUtilities.dp(16);
        flashLp.gravity = Gravity.TOP | Gravity.RIGHT;

        FrameLayout.LayoutParams switchLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        switchLp.bottomMargin = AndroidUtilities.dp(120);
        switchLp.gravity = Gravity.BOTTOM;

        FrameLayout.LayoutParams cameraControlsLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, AndroidUtilities.dp(100));
        cameraControlsLp.gravity = Gravity.BOTTOM;

        flash = new FlashModeButton(getContext());
        sw = new ModeSwitch(getContext());
        controls = new CameraControlLayout(getContext());
        controls.setController(this);

        flash.setLayoutParams(flashLp);
        flash.setController(this);
        sw.setLayoutParams(switchLp);
        controls.setLayoutParams(cameraControlsLp);

        final ViewConfiguration config = ViewConfiguration.get(getActivity());
        ModeSwitch.Callback cb = new ModeSwitch.Callback() {
            @Override
            public void onLeftSide() {
                controls.capture.switchToCaptureMode();
                didModeSwitchRequested(CameraMode.STILL);
            }

            @Override
            public void onRightSide() {
                controls.capture.switchToVideoMode();
                didModeSwitchRequested(CameraMode.VIDEO);
            }
        };
        sw.setCallback(cb);
        final GestureDetector.OnGestureListener gestureListener = new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2,
                                   float velocityX, float velocityY) {
                int touchX = (int) e1.getRawX();
                int x = (int) e2.getRawX();
                if (velocityX > config.getScaledMinimumFlingVelocity()) {
                    sw.animateMove(touchX, x);
                    return true;
                }
                return false;
            }
        };
        final GestureDetector detector = new GestureDetector(getActivity(), gestureListener);
        finder.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(!canSwipe())
                    return false;
                detector.onTouchEvent(motionEvent);
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    touchX = (int) motionEvent.getRawX();
                    return true;
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    sw.move(touchX, (int) motionEvent.getRawX());
                    return true;
                }
                else if(motionEvent.getAction() == MotionEvent.ACTION_CANCEL || motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    sw.onFinishMove(touchX, (int) motionEvent.getRawX());
                    return true;
                }
                return false;
            }
        });
        viewsContainer.addView(flash);
        viewsContainer.addView(sw);
        viewsContainer.addView(controls);
        viewsContainer.addView(timer);
        return viewsContainer;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(playingVideo) {
            videoContainer.setBackgroundColor(0xFF333333);
            //videoView.setZOrderMediaOverlay(true);
            videoView.start();
        }
        NotificationCenter.getInstance().addObserver(onPictureTaken);
        NotificationCenter.getInstance().addObserver(onPictureTmpSaved);
        NotificationCenter.getInstance().addObserver(videoRecFinished);
        NotificationCenter.getInstance().addObserver(videoProcessed);
        NotificationCenter.getInstance().addObserver(flashInfo);
        NotificationCenter.getInstance().addObserver(camCountObserver);
        if(!editMode && !playingVideo && videoPreview == null && bmpRef == null)
        {
            if(getCurrentCameraMode() == CameraMode.VIDEO)
                VideoCaptureHelper.sharedInstance().onUiResume();
            else
                StillCaptureHelper.sharedInstance().onUiResume();
        }
    }
    public void onPause()
    {
        super.onPause();
        if(playingVideo)
            videoView.pause();
        NotificationCenter.getInstance().removeObserver(onPictureTaken);
        NotificationCenter.getInstance().removeObserver(onPictureTmpSaved);
        NotificationCenter.getInstance().removeObserver(videoRecFinished);
        NotificationCenter.getInstance().removeObserver(videoProcessed);
        NotificationCenter.getInstance().removeObserver(flashInfo);
        NotificationCenter.getInstance().removeObserver(camCountObserver);
        if(editMode) {
            if(getCurrentCameraMode() == CameraMode.VIDEO)
                VideoCaptureHelper.sharedInstance().onUiPause(null);
            else
                StillCaptureHelper.sharedInstance().onUiPause(null);
        }
    }

    private void switchToVideo()
    {
        mode = CameraMode.VIDEO;
        StillCaptureHelper.sharedInstance().onUiPause(turnVideoOn);
    }
    private void switchToStillCapture()
    {
        mode = CameraMode.STILL;
        VideoCaptureHelper.sharedInstance().onUiPause(turnStillCaptureOn);
    }
    private void closeSaveScreen()
    {
        if(getCurrentCameraMode() != CameraMode.VIDEO) {
            StillCaptureHelper.sharedInstance().onUiResume();
            onShoot.imageView.setImageDrawable(null);
            editMode = false;
            onShoot.setVisibility(View.GONE);
            if(bmpRef != null && !bmpRef.isRecycled())
            {
                bmpRef.recycle();
                bmpRef = null;
                bmpDrawable = null;
                System.gc();
            }
        }
        else {
            VideoCaptureHelper.sharedInstance().onUiResume();
        }
        //photoViewAttacher.update();
        finder.setVisibility(View.VISIBLE);
    }
    private void movePhotoToGallery()
    {
        /*if(tmpFile == null)
            return;*/
        savingPd.show();
        Log.d("CameraFragment", "DisplayRect:".concat(photoViewAttacher.getDisplayRect().toString()));
        final int viewWidth = photoViewAttacher.getImageView().getMeasuredWidth();
        final int viewHeight = photoViewAttacher.getImageView().getMeasuredHeight();
        final float rotation = Math.abs(photoViewAttacher.getRotation());
        int x0, y0, width, height;
        float reduceHeight = 0.0f;
        if(edited && editButtonTouched)
        {
            float[] drawMatrix = new float[9];
            Rect cropRegion = new Rect(onShoot.cropView.getCropRegion());
            RectF displayRect = photoViewAttacher.getDisplayRect();
            if(displayRect != null && displayRect.top > 0.0f)
            {
                if(cropRegion.top < (int) displayRect.top) {
                    cropRegion.top = 0;
                    reduceHeight += displayRect.top;
                }
                if(cropRegion.bottom > displayRect.bottom) {
                    cropRegion.bottom = (int) displayRect.bottom;
                    reduceHeight += viewHeight - displayRect.bottom;
                }
                Log.d("Fragment", String.format("reduceHeight = %.2f", reduceHeight));
            }
            photoViewAttacher.getDrawMatrix().getValues(drawMatrix);
            int absX = (int) Math.abs(drawMatrix[Matrix.MTRANS_X]);
            int absY = displayRect != null && displayRect.top > 0 ? (int) Math.abs(drawMatrix[Matrix.MTRANS_Y] - displayRect.top) : (int) Math.abs(drawMatrix[Matrix.MTRANS_Y]);

            float scale = (float) Math.sqrt(Math.pow(drawMatrix[Matrix.MSCALE_X], 2) + Math.pow(drawMatrix[Matrix.MSKEW_Y], 2));

            if(rotation == 0.0f)
            {
                x0 = (int) (absX / scale);
                y0 = (int) (absY / scale);
                x0 += (int) (((float) cropRegion.left) / scale);
                y0 += (int) (((float) cropRegion.top) / scale);
            }
            else if(rotation == 90.0f)
            {
                y0 = (int) (absX / scale);
                x0 = (int) ((absY - viewHeight) / scale);
                x0 += (int) ((float)(viewHeight - cropRegion.bottom) / scale);
                y0 += (int) ((float)(cropRegion.left) / scale);
            }
            else if(rotation == 180.0f)
            {
                x0 = (int) ((absX - viewWidth) / scale);
                y0 = (int) ((absY - viewHeight) / scale);
                x0 += (int) ((float)(viewWidth - cropRegion.right) / scale);
                y0 += (int) ((float)(viewHeight - cropRegion.bottom) / scale);
            }
            else if(rotation == 270.0f)
            {
                y0 = (int) ((absX - viewWidth) / scale);
                x0 = (int) (absY / scale);
                x0 += (int) (((float) cropRegion.top) / scale);
                y0 += (int) ((float)(viewWidth - cropRegion.right) / scale);
            }
            else
                x0 = y0 = 0;

            width = (int) (((float) cropRegion.width()) / scale);
            height = (int) (((float) cropRegion.height()) / scale);
            reduceHeight /= scale;
            if(height > (int) reduceHeight)
                height -= (int) reduceHeight;

            if(rotation == 90.0f || rotation == 270.0f)
            {
                int tmp = width;
                width = height;
                height = tmp;
            }

            if(x0 < 0)
                x0 = 0;
            if(y0 < 0)
                y0 = 0;
            if(x0+width > bmpRef.getWidth()) {
                int dw = x0 + width - bmpRef.getWidth();
                width -= dw;
            }
            else if(y0 + height > bmpRef.getHeight()) {
                int dh = y0 + height - bmpRef.getHeight();
                height -= dh;
            }
        }
        else
        {
            width = bmpRef.getWidth();
            height = bmpRef.getHeight();
            x0 = y0 = 0;
        }
        if(BuildConfig.DEBUG)
            Log.d("Fragment", String.format("Edit info: rotation = %.2f; x0 = %d; y0 = %d; width = %d; height = %d", rotation, x0, y0, width, height));

        final int finalWidth = width;
        final int finalHeight = height;
        final int finalX = x0;
        final int finalY = y0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap finalBitmap = null;
                try
                {
                    System.gc();
                    if(edited)
                    {
                        //rotation
                        Matrix m = new Matrix();
                        m.postRotate(rotation, finalWidth / 2, finalHeight / 2);
                        finalBitmap = Bitmap.createBitmap(bmpRef, finalX, finalY, finalWidth, finalHeight, m, true);
                    }
                    else
                        finalBitmap = bmpRef;
                    MediaStore.Images.Media.insertImage(AppLoader.sharedInstance().getContentResolver(), finalBitmap, String.format(Locale.US, "NiceCamera_%d", System.currentTimeMillis()), "");
                    System.gc();
                    AppLoader.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            onShoot.setEditMode(false);
                            controls.setVisibility(View.VISIBLE);
                            savingPd.dismiss();
                            photoViewAttacher.resetRotationValue();
                            closeSaveScreen();
                            controls.hideDecisionButtons();
                            Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), R.string.saved_to_gallery, Toast.LENGTH_SHORT).show();
                        }
                    });
                    //tmpFile = null;
                }
                catch(Exception e) { e.printStackTrace(); }
                finally {
                    editButtonTouched = false;
                    if(finalBitmap != null && edited)
                        finalBitmap.recycle();
                    if(bmpRef != null && !bmpRef.isRecycled())
                    {
                        bmpRef.recycle();
                        bmpRef = null;
                        bmpDrawable = null;
                        System.gc();
                    }
                }
            }
        }).start();
    }
    private void removeVideo()
    {
        try
        {
            File f = new File(pathToVideo);
            f.delete();
        }
        catch(Exception e) { e.printStackTrace(); }
    }

    private boolean canSwipe()
    {
        return !(editMode || pathToVideo != null || isProcessing() || controls.capture.isAnimating() || recording || controls.swap.isAnimating());
    }

    private void resetAllowFlags()
    {
        savePhotoAllowed = saveVideoAllowed = false;
    }

    private void goToSavePhoto()
    {
        if(savePhotoAllowed && !isProcessing() && bmpRef != null) {
            try
            {
                ((MainActivity)getActivity()).setKeepScreenOn(false);
                ((MainActivity) getActivity()).interceptBackPress(decisionBackPress);
                controls.actionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editButtonTouched = true;
                        controls.setVisibility(View.GONE);
                        onShoot.setEditMode(true);
                        photoViewAttacher.scaleToNaturalSize();
                    }
                });
            }
            catch(Exception ignored) {}
            controls.actionButton.setImageResource(R.drawable.crop);
            controls.showDecisionButtons();
            StillCaptureHelper.sharedInstance().onUiPause(null);
            bmpDrawable = new FastBitmapDrawable(bmpRef);
            onShoot.imageView.setImageDrawable(bmpDrawable);
            onShoot.cropView.setAspectRatio(bmpRef.getWidth(), bmpRef.getHeight());

            photoViewAttacher.update();
            onShoot.setVisibility(View.VISIBLE);
            editMode = true;
            finder.setVisibility(View.GONE);
        }
    }
    private void goToSaveVideo()
    {
        if(saveVideoAllowed && !isProcessing() && pathToVideo != null) {
            try
            {
                ((MainActivity)getActivity()).setKeepScreenOn(false);
                ((MainActivity) getActivity()).interceptBackPress(decisionBackPress);
            }
            catch(Exception ignored) {}
            controls.actionButton.setImageResource(R.drawable.video_play);
            controls.showDecisionButtons();
            controls.allowClickEvent();
            videoContainer.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.VISIBLE);
            VideoCaptureHelper.sharedInstance().onUiPause(null);
            controls.actionButton.setOnClickListener(playPauseClick);
            Log.d("Fragment", String.format("videoPath = %s", pathToVideo == null ? "null" : pathToVideo));
            videoView.setVideoPath(pathToVideo);
            videoView.setMediaController(null);
            videoView.setOnCompletionListener(videoFinish);
            videoView.setOnErrorListener(videoError);
            videoView.setOnPreparedListener(mayStartVideo);
        }
    }

    @Override
    public void didCaptureRequested() {
        resetAllowFlags();
        sw.hide();
        flash.setVisibility(View.GONE);
        processing = true;
        StillCaptureHelper.sharedInstance().takePicture();
    }

    @Override
    public void didPhotoSaveAllowed() {
        savePhotoAllowed = true;
        goToSavePhoto();
    }

    @Override
    public void didModeSwitchRequested(CameraMode mode) {
        this.mode = mode;
        if(mode != CameraMode.STILL) {
            flash.setVisibility(View.GONE);
            switchToVideo();
        }
        else {
            flash.onFlashInfo(hasFlash);
            switchToStillCapture();
        }
    }

    @Override
    public void didLongtapRecordStartRequested() {
        resetAllowFlags();
        sw.hide();
        flash.setVisibility(View.GONE);
        timer.show();
        longtapRecording = true;
        VideoCaptureHelper.sharedInstance().startLongtapRecording();
    }

    @Override
    public void didLongtapRecordStopRequested() {
        if(processing)
            return;
        timer.hide();
        processing = true;
        VideoCaptureHelper.sharedInstance().stopLongtapRecording();
    }

    @Override
    public void didVideoSaveAllowed() {
        saveVideoAllowed = true;
        if(failed)
            restoreCamera();
        else
            goToSaveVideo();
    }

    @Override
    public void didRecordStartRequested() {
        resetAllowFlags();
        sw.hide();
        timer.show();
        recording = true;
        VideoCaptureHelper.sharedInstance().toggleRecord();
    }

    @Override
    public void didRecordStopRequested() {
        timer.hide();
        recording = false;
        processing = true;
        VideoCaptureHelper.sharedInstance().toggleRecord();
    }

    @Override
    public void didSwapCameraRequested() {
        if(camerasCount < 2) {
            Toast.makeText(getContext(), R.string.no_second_camera, Toast.LENGTH_SHORT).show();
            return;
        }
        if(getCurrentCameraMode() == CameraMode.VIDEO)
            VideoCaptureHelper.sharedInstance().swapCamera();
        else
            StillCaptureHelper.sharedInstance().swapCamera();
    }

    @Override
    public void didFlashModeChangeRequested(FlashMode mode) {
        StillCaptureHelper.sharedInstance().setFlashMode(mode);
    }

    @Override
    public void onDismiss() {
        editButtonTouched = false;
        try
        {
            ((MainActivity) getActivity()).removeBackPressInterceptor(decisionBackPress);
            ((MainActivity)getActivity()).setKeepScreenOn(true);
        }
        catch(Exception ignored) {}
        closeSaveScreen();
        sw.show();
        if(getCurrentCameraMode() == CameraMode.STILL && !longtapRecording)
            flash.onFlashInfo(hasFlash);
        else
        {
            if(playingVideo)
            {
                playingVideo = false;
                if(videoView.isPlaying()) {
                    videoView.pause();
                    videoView.stopPlayback();
                }
            }
            longtapRecording = false;
            removeVideo();
            pathToVideo = null;
            prepared = false;
            videoContainer.setBackgroundColor(0xFF333333);
            videoContainer.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            if(videoPreview != null && videoPreview.getBitmap() != null && !videoPreview.getBitmap().isRecycled())
                videoPreview.getBitmap().recycle();
            videoPreview = null;
        }
    }

    @Override
    public void onAccept() {
        try
        {
            ((MainActivity) getActivity()).removeBackPressInterceptor(decisionBackPress);
            ((MainActivity)getActivity()).setKeepScreenOn(true);
        }
        catch(Exception ignored) {}
        if(getCurrentCameraMode() == CameraMode.STILL && !longtapRecording)
        {
            NotificationCenter.getInstance().addObserver(storagePermissionGranted);
            ((MainActivity) getActivity()).requestStoragePermission();
        }
        else
        {
            if(playingVideo)
            {
                playingVideo = false;
                if(videoView.isPlaying()) {
                    videoView.pause();
                    videoView.stopPlayback();
                }
            }
            closeSaveScreen();
            controls.hideDecisionButtons();
            sw.show();
            if(pathToVideo != null)
            {
                try
                {
                    getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(pathToVideo))));
                }
                catch(Exception e) { e.printStackTrace(); }
            }
            pathToVideo = null;
            prepared = false;
            videoContainer.setBackgroundColor(0xFF333333);
            videoContainer.setVisibility(View.GONE);
            videoView.setVisibility(View.GONE);
            if(videoPreview != null && !videoPreview.getBitmap().isRecycled())
            {
                videoPreview.getBitmap().recycle();
                videoPreview = null;
            }
        }
    }

    @Override
    public boolean isRecording() {
        return recording;
    }

    @Override
    public boolean isLongtapRecording() {
        return longtapRecording;
    }

    @Override
    public boolean isProcessing() {
        return processing;
    }

    @Override
    public CameraMode getCurrentCameraMode() {
        return mode;
    }

    private void restoreCamera()
    {
        failed = false;
        if(longtapRecording || recording)
        {
            Log.d("Fragment", "restore");
            longtapRecording = false;
            recording = false;
            AppLoader.applicationHandler.postDelayed(restoreRunnable, 300);
        }
    }
    @Override
    public void didCancelRequested() {
        edited = false;
        editButtonTouched = false;
        photoViewAttacher.resetRotation();
        onShoot.setEditMode(false);
        controls.setVisibility(View.VISIBLE);
    }

    @Override
    public void didResetRequested() {
        edited = false;
        photoViewAttacher.resetRotation();
    }

    @Override
    public void didRotateRequested() {
        edited = true;
        photoViewAttacher.rotateCcw();
    }
    @Override
    public void didCropMoved() {
        edited = true;
    }

    @Override
    public void didFinishRequested() {
        movePhotoToGallery();
    }

    @Override
    public void didPhotoRectChanged() {
        edited = true;
    }

    @Override
    public void didPhotoRectNormalized() {
        edited = false;
    }
}
