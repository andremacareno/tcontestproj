package com.andremacareno.tcontestproj;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.File;
import java.util.Hashtable;

/**
 * Created by Andrew on 11.05.2015.
 */
public class AndroidUtilities {
    public static float density = AppLoader.sharedInstance().getResources().getDisplayMetrics().density;
    private static final Hashtable<String, Typeface> typefaceCache = new Hashtable<>();
    private static int statusBarHeight = 0;
    private static Point screen = null;
    public static void showKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager inputManager = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static boolean isKeyboardShown(View view) {
        if (view == null) {
            return false;
        }
        InputMethodManager inputManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        return inputManager.isActive(view);
    }

    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!imm.isActive()) {
            return;
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static int dp(int pixels)
    {
        if (pixels == 0) {
            return 0;
        }
        return (int)Math.ceil(density * pixels);
    }
    public static int screenWidthInDp(int widthPixels)
    {
        return (int) Math.ceil(widthPixels / density);
    }
    public static Typeface getTypeface(String assetPath) {
        synchronized (typefaceCache) {
            if (!typefaceCache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(AppLoader.sharedInstance().getAssets(), assetPath);
                    typefaceCache.put(assetPath, t);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return typefaceCache.get(assetPath);
        }
    }
    public static int getStatusBarHeight() {
        if(Build.VERSION.SDK_INT < 19)
            return 0;
        if(statusBarHeight != 0)
            return statusBarHeight;
        int resourceId = AppLoader.sharedInstance().getApplicationContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = AppLoader.sharedInstance().getApplicationContext().getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }
    public static boolean isLowDPIScreen() {
        DisplayMetrics metrics = AppLoader.sharedInstance().getResources().getDisplayMetrics();
        if(metrics.densityDpi == DisplayMetrics.DENSITY_LOW || metrics.densityDpi == DisplayMetrics.DENSITY_MEDIUM)
            return true;
        return false;
    }
    public static boolean isHdpi() {
        DisplayMetrics metrics = AppLoader.sharedInstance().getResources().getDisplayMetrics();
        return metrics.densityDpi == DisplayMetrics.DENSITY_HIGH;
    }
    public static WindowManager getWindowManager()
    {
        return (WindowManager) AppLoader.sharedInstance().getSystemService(Context.WINDOW_SERVICE);
    }
    public static File getInternalFolder()
    {
        try
        {
            return AppLoader.sharedInstance().getExternalCacheDir();
        }
        catch(Exception ignored) { return null; }
    }
    public static File getVideoFolder()
    {
        try
        {
            File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            File appFolder = new File(root, "NiceCamera");
            if (! appFolder.exists()){
                if (! appFolder.mkdirs()){
                    return null;
                }
            }
            return appFolder;
        }
        catch(Exception ignored) { return null; }
    }


}
