package com.andremacareno.tcontestproj;

/**
 * Created by Andrew on 01.05.2015.
 */
public interface BackPressListener {
    public void onBackPressed();
}
