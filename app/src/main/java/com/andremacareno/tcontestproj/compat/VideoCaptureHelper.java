package com.andremacareno.tcontestproj.compat;

import android.os.Build;
import android.view.View;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by andremacareno on 27/04/16.
 */
public abstract class VideoCaptureHelper {
    private static VideoCaptureHelper _instance;
    protected View attachedTextureView;
    protected final AtomicBoolean recording = new AtomicBoolean(false);
    protected final AtomicBoolean longtapRecording = new AtomicBoolean(false);
    protected VideoCaptureHelper()
    {

    }
    public static VideoCaptureHelper sharedInstance() {
        if(_instance == null)
        {
            synchronized(VideoCaptureHelper.class)
            {
                //_instance = new PreLollipopVideoCam();
                if(_instance == null)
                    _instance = Build.VERSION.SDK_INT >= 21 ? new LollipopVideoCam() : new PreLollipopVideoCam();
            }
        }
        return _instance;
    }
    public void attachTextureView(View tv) {
        this.attachedTextureView = tv;
    }

    public void toggleRecord()
    {
        synchronized(recording)
        {
            if(recording.get())
                stopRecording();
            else
                startRecording();
        }
    }
    protected abstract void openCamera(int width, int height, boolean front);
    protected abstract void closeCamera();
    public abstract void swapCamera();
    public abstract void onUiResume();
    public abstract void onUiPause(Runnable onFinish);
    public abstract void startLongtapRecording();
    public abstract void stopLongtapRecording();
    protected abstract void startRecording();
    protected abstract void stopRecording();


}
