package com.andremacareno.tcontestproj.compat;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.AppLoader;
import com.andremacareno.tcontestproj.BuildConfig;
import com.andremacareno.tcontestproj.views.AutoFitSurfaceView;
import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by andremacareno on 27/04/16.
 */
public class PreLollipopVideoCam extends VideoCaptureHelper implements SurfaceHolder.Callback {

    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();
    private static final String TAG = "PreLollipopVideoCam";


    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }
    private int frontCam = -1, rearCam = -1;
    private static final HandlerThread ht = new HandlerThread("CameraOpenHandlerThread");
    static {
        ht.start();
    }
    private static Handler backgroundHandler = new Handler(ht.getLooper());
    private Camera mCamera;


    /**
     * The {@link android.util.Size} of camera preview.
     */
    private Camera.Size mPreviewSize;

    /**
     * The {@link android.util.Size} of video recording.
     */
    private Camera.Size mVideoSize;

    /**
     * MediaRecorder
     */
    private MediaRecorder mMediaRecorder;

    Camera.ErrorCallback errorCallback = new Camera.ErrorCallback() {
        @Override
        public void onError(int i, Camera camera) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "onError");
            if(i == Camera.CAMERA_ERROR_SERVER_DIED) {
                performCloseCamera(new Runnable() {
                    @Override
                    public void run() {
                        openCamera(attachedTextureView.getMeasuredWidth(), attachedTextureView.getMeasuredHeight(), StillCaptureHelper.activeCam == StillCaptureHelper.ActiveCam.FRONT);
                    }
                });
            }

        }
    };
    private final Runnable longtapStartRunnable = new Runnable() {
        @Override
        public void run() {
            onUiResume();
        }
    };
    private final Runnable closeCameraRunnable = new Runnable() {
        @Override
        public void run() {
            if(mCamera != null)
            {
                if(attachedTextureView != null)
                    ((SurfaceView) attachedTextureView).getHolder().removeCallback(PreLollipopVideoCam.this);
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
            }
        }
    };
    private final Runnable swapCamRunnable = new Runnable() {
        @Override
        public void run() {
            if(attachedTextureView != null) {
                ((SurfaceView) attachedTextureView).getHolder().addCallback(PreLollipopVideoCam.this);
                openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), StillCaptureHelper.activeCam == StillCaptureHelper.ActiveCam.REAR);
            }
        }
    };

    private Integer mSensorOrientation;
    private String mNextVideoAbsolutePath;

    private static Camera.Size chooseVideoSize(List<Camera.Size> choices, Camera.Size uBound) {
        Camera.Size max = null;

        for (Camera.Size size : choices) {
            if(size.width > uBound.width || size.height > uBound.height)
                continue;
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("supports video %d x %d", size.width, size.height));
            // && size.getWidth() <= 1080
            if(max == null)
                max = size;
            else if(size.width * size.height > max.width * max.height)
                max = size;

            /*if (size.width == size.height * 16 / 9) {
                if(max == null)
                    max = size;
                else if(size.width * size.height > max.width * max.height)
                    max = size;
            }*/
        }
        if(max != null)
            return max;
        Log.e(TAG, "Couldn't find any suitable video size");
        return choices.get(choices.size() - 1);
    }

    private static Camera.Size chooseOptimalSize(List<Camera.Size> choices, int width, int height, Camera.Size uBound) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Camera.Size> bigEnough = new ArrayList<Camera.Size>();
        Camera.Size max = null;
        int w = uBound.width;
        int h = uBound.height;
        for (Camera.Size option : choices) {
            if(option.height > h || option.width > w)
                continue;
            if (option.height == option.height * h / w &&
                    option.width >= width && option.height >= height) {
                bigEnough.add(option);
            }
            else if(option.height == option.width * h / w && (max == null || max.height * max.width < option.width * option.height))
                max = option;
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        }
        else if(max != null)
            return max;
        else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices.get(0);
        }
    }
    @Override
    public void startLongtapRecording()
    {
        synchronized(longtapRecording)
        {
            longtapRecording.set(true);
        }
        ((PreLollipopCam) StillCaptureHelper.sharedInstance()).performCloseCamera(longtapStartRunnable);
    }
    @Override
    public void stopLongtapRecording()
    {
        stopRecording();
    }
    @Override
    public void onUiResume() {
        if(attachedTextureView != null)
            ((SurfaceView) attachedTextureView).getHolder().addCallback(this);
        openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), StillCaptureHelper.activeCam == StillCaptureHelper.ActiveCam.FRONT);
    }

    @Override
    public void onUiPause(Runnable onFinish) {
        performCloseCamera(onFinish);
    }


    private void obtainCameraInfo()
    {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();
        if(BuildConfig.DEBUG)
            Log.d(TAG, String.format("found %d cameras", cameraCount));
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                frontCam = camIdx;
            else if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                rearCam = camIdx;
        }

    }
    private void performOpenCamera(final int width, final int height, boolean front)
    {
        if(rearCam == frontCam && rearCam == -1)
            obtainCameraInfo();
        try {
            mCamera = Camera.open(front ? frontCam : rearCam);
            mCamera.setErrorCallback(errorCallback);
            Camera.Parameters parameters = mCamera.getParameters();
            CamcorderProfile hd = CamcorderProfile.get(front ? frontCam : rearCam, CamcorderProfile.QUALITY_HIGH);
            Camera.Size hdSize = mCamera.new Size(hd.videoFrameWidth, hd.videoFrameHeight);
            if(BuildConfig.DEBUG)
            {
                Log.d(TAG, String.format("HD: %d x %d", hd.videoFrameWidth, hd.videoFrameHeight));
                for(Camera.Size s : parameters.getSupportedPreviewSizes())
                {
                    Log.d(TAG, String.format("supports %d x %d", s.width, s.height));
                }
            }

            if(parameters.getSupportedVideoSizes() == null) {
                if(BuildConfig.DEBUG)
                    Log.d(TAG, "null supported videoSizes");
                mVideoSize = chooseVideoSize(parameters.getSupportedPreviewSizes(), hdSize);
            }
            else
                mVideoSize = chooseVideoSize(parameters.getSupportedVideoSizes(), hdSize);
            mPreviewSize = chooseOptimalSize(parameters.getSupportedPreviewSizes(),
                    width, height, mVideoSize);
            Log.d(TAG, String.format("previewSize: %d x %d; video size: %d x %d", mPreviewSize.width, mPreviewSize.height, mVideoSize.width, mVideoSize.height));
            //int displayRotation = AndroidUtilities.getWindowManager().getDefaultDisplay().getRotation();
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(front ? frontCam : rearCam, info);
            mSensorOrientation = info.orientation;
            final int orientation = AppLoader.sharedInstance().getResources().getConfiguration().orientation;
            mCamera.setDisplayOrientation(mSensorOrientation == 270 && front ? 90 : mSensorOrientation);
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    //configureTransform(width, height);
                    try
                    {
                        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            ((AutoFitSurfaceView)attachedTextureView).setAspectRatio(mPreviewSize.width, mPreviewSize.height);
                        } else {
                            ((AutoFitSurfaceView)attachedTextureView).setAspectRatio(mPreviewSize.height, mPreviewSize.width);
                        }
                    }
                    catch(Exception ignored) {
                        Crashlytics.getInstance().core.logException(ignored);}
                }
            });
            StillCaptureHelper.activeCam = front ? StillCaptureHelper.ActiveCam.FRONT : StillCaptureHelper.ActiveCam.REAR;
            parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            mCamera.setParameters(parameters);
            StillCaptureHelper.activeCam = front ? StillCaptureHelper.ActiveCam.FRONT : StillCaptureHelper.ActiveCam.REAR;
            //mCamera.setPreviewTexture(attachedTextureView.getSurfaceTexture());
            mCamera.setPreviewDisplay(((SurfaceView) attachedTextureView).getHolder());
            mCamera.startPreview();
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    configureTransform(attachedTextureView.getWidth(), attachedTextureView.getHeight());
                    synchronized(recording)
                    {
                        if(longtapRecording.get() && !recording.get())
                            startRecording();
                    }
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "failed to open Camera");
            e.printStackTrace();
        }
    }
    @Override
    protected void openCamera(final int width, final int height, final boolean front) {
        performCloseCamera(new Runnable() {
            @Override
            public void run() {
                backgroundHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        performOpenCamera(width, height, front);
                    }
                });
            }
        });
    }

    private void performCloseCamera(final Runnable afterClose)
    {
        backgroundHandler.post(new Runnable() {
            @Override
            public void run() {
                closeCameraRunnable.run();
                if(afterClose != null)
                {
                    AppLoader.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            afterClose.run();
                        }
                    });
                }
            }
        });
    }
    @Override
    public void closeCamera() {
        performCloseCamera(null);
    }

    @Override
    public void swapCamera() {
        performCloseCamera(swapCamRunnable);
    }


    private void configureTransform(int viewWidth, int viewHeight) {
        if (null == attachedTextureView || null == mPreviewSize) {
            return;
        }
        int rotation = ((WindowManager) AppLoader.sharedInstance().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.height, mPreviewSize.height);
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.height,
                    (float) viewWidth / mPreviewSize.width);
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        //attachedTextureView.setTransform(matrix);
    }
    private void releaseRecorder()
    {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
            mCamera.lock();           // lock camera for later use
        }
    }
    private void setUpMediaRecorder() throws IOException {
        if(BuildConfig.DEBUG)
        {
            if(mCamera.getParameters().getSupportedVideoSizes() == null)
            {
                Log.d(TAG, String.format("getSupportedVideoSizes() = null"));
            }
            else
            {
                for(Camera.Size s : mCamera.getParameters().getSupportedVideoSizes())
                    Log.d(TAG, String.format("supports video %d x %d", s.width, s.height));
            }
        }
        if(mMediaRecorder != null)
            releaseRecorder();
        mMediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mMediaRecorder.setProfile(CamcorderProfile.get(StillCaptureHelper.activeCam == StillCaptureHelper.ActiveCam.FRONT ? frontCam : rearCam, CamcorderProfile.QUALITY_HIGH));
        if (mNextVideoAbsolutePath == null || mNextVideoAbsolutePath.isEmpty()) {
            mNextVideoAbsolutePath = getVideoFilePath();
        }
        mMediaRecorder.setOutputFile(mNextVideoAbsolutePath);
        mMediaRecorder.setPreviewDisplay(((SurfaceView) attachedTextureView).getHolder().getSurface());
        /*mMediaRecorder.setVideoEncodingBitRate(10000000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(mVideoSize.width, mVideoSize.height);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);*/
        int rotation = ((WindowManager) AppLoader.sharedInstance().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        switch (mSensorOrientation) {
            case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                mMediaRecorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
                break;
            case SENSOR_ORIENTATION_INVERSE_DEGREES:
                mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
                break;
        }
        mMediaRecorder.prepare();
    }

    private String getVideoFilePath() {
        return AndroidUtilities.getVideoFolder().getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".mp4";
    }

    @Override
    protected void startRecording() {
        if (null == mCamera || attachedTextureView == null|| null == mPreviewSize) {
            return;
        }
        try {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "startRecording");
            setUpMediaRecorder();
            /*SurfaceTexture texture = attachedTextureView.getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.width, mPreviewSize.height);*/

            // Set up Surface for the MediaRecorder
            mMediaRecorder.start();
            synchronized(recording)
            {
                recording.set(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Override
    protected void stopRecording() {
        // UI
        boolean sendNull = false;
        try
        {
            synchronized(recording)
            {
                if(!recording.get())
                    throw new IllegalStateException("calling stop() before MediaRecorder start");
            }
            // Stop recording
            mMediaRecorder.stop();
            releaseRecorder();
            synchronized(recording)
            {
                recording.set(false);
            }
            final String path = mNextVideoAbsolutePath;
            NotificationCenter.getInstance().postNotification(NotificationCenter.didVideoRecorded, path);
            backgroundHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "creating thumbnail");
                    Bitmap bmp;
                    try
                    {
                        bmp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);
                        //TODO fade?
                    }
                    catch(Exception e) { e.printStackTrace(); bmp = null; }
                    NotificationCenter.getInstance().postNotification(NotificationCenter.didVideoProcessed, bmp);
                }
            });
            mNextVideoAbsolutePath = null;
            synchronized(longtapRecording)
            {
                if(!longtapRecording.get())
                    mCamera.startPreview();
            }
        }
        catch(Exception e) { e.printStackTrace(); sendNull = true;  }
        if(sendNull)
            NotificationCenter.getInstance().postNotification(NotificationCenter.didVideoRecorded, null);
        synchronized(longtapRecording)
        {
            if(longtapRecording.get())
            {
                longtapRecording.set(false);
                final boolean finalNull = sendNull;
                performCloseCamera(new Runnable() {
                    @Override
                    public void run() {
                        if(finalNull)
                            StillCaptureHelper.sharedInstance().onUiResume();
                    }
                });

            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        if(mCamera != null)
        {
            try
            {
                if(mPreviewSize == null)
                    return;
                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
                final int orientation = AppLoader.sharedInstance().getResources().getConfiguration().orientation;
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try
                        {
                            if(mPreviewSize == null)
                                return;
                            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                                ((AutoFitSurfaceView) attachedTextureView).setAspectRatio(
                                        mPreviewSize.width, mPreviewSize.height);
                            } else {
                                ((AutoFitSurfaceView) attachedTextureView).setAspectRatio(
                                        mPreviewSize.height, mPreviewSize.width);
                            }
                        }
                        catch(Exception ignored) {}
                    }
                });
                mCamera.setParameters(parameters);

                // Important: Call startPreview() to start updating the preview surface.
                // Preview must be started before you can take a picture.
                mCamera.startPreview();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        closeCamera();
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Camera.Size> {

        @Override
        public int compare(Camera.Size lhs, Camera.Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.width * lhs.height -
                    (long) rhs.width * rhs.height);
        }

    }
}
