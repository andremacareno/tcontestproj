package com.andremacareno.tcontestproj.compat;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.AppLoader;
import com.andremacareno.tcontestproj.BuildConfig;
import com.andremacareno.tcontestproj.views.AutoFitSurfaceView;
import com.andremacareno.tcontestproj.views.control.FlashMode;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by andremacareno on 27/04/16.
 */
public class PreLollipopCam extends StillCaptureHelper implements SurfaceHolder.Callback {
    private Camera mCamera;
    private int frontCam = -1, rearCam = -1;
    private static final int MAX_PREVIEW_WIDTH = 1920;
    private static final int MAX_PREVIEW_HEIGHT = 1080;
    private static final String TAG = "PreLollipopCam";

    private static final HandlerThread ht = new HandlerThread("CameraOpenHandlerThread");
    static {
        ht.start();
    }
    private static Handler bgHandler = new Handler(ht.getLooper());
    private Camera.Size mPreviewSize;
    Matrix m = new Matrix();
    private final Camera.AutoFocusCallback af = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean b, Camera camera) {
            camera.takePicture(null, null, jpegCallback);
        }
    };
    private final Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] bytes, Camera camera) {
            try
            {
                Camera.Size picSize = camera.getParameters().getPictureSize();
                if(BuildConfig.DEBUG)
                    Log.d(TAG, String.format("pictureSize: %d x %d", picSize.width, picSize.height));
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inSampleSize = calculateInSampleSize(picSize.width, picSize.height);
                opts.inJustDecodeBounds = false;
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opts);
                /*if(BuildConfig.DEBUG)
                    Log.d(TAG, String.format("outWidth = %d; outHeight = %d", opts.outWidth, opts.outHeight));
                Bitmap finalBmp = Bitmap.createBitmap(bmp, 0, 0, opts.outWidth, opts.outHeight, m, true);
                bmp.recycle();*/
                NotificationCenter.getInstance().postNotification(NotificationCenter.didPictureTaken, bmp);
            }
            catch(Exception e) { Crashlytics.getInstance().core.logException(e); e.printStackTrace();  }
        }
    };
    private final Runnable closeCameraRunnable = new Runnable() {
        @Override
        public void run() {
            if(mCamera != null)
            {
                ((SurfaceView) attachedTextureView).getHolder().removeCallback(PreLollipopCam.this);
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
            }
        }
    };
    private final Runnable swapCamRunnable = new Runnable() {
        @Override
        public void run() {
            if(attachedTextureView.getWidth() == 0)
                ((SurfaceView) attachedTextureView).getHolder().addCallback(PreLollipopCam.this);
            else
                openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), activeCam == ActiveCam.REAR);
        }
    };

    private void obtainCameraInfo()
    {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras();
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                frontCam = camIdx;
            else if(cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                rearCam = camIdx;
        }

    }
    @Override
    protected void openCamera(final int width, final int height, final boolean front) {
        performCloseCamera(new Runnable() {
            @Override
            public void run() {
                bgHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        performOpenCamera(width, height, front);
                    }
                });
            }
        });
    }
    private void performOpenCamera(final int width, final int height, boolean front)
    {
        if(rearCam == frontCam && rearCam == -1)
            obtainCameraInfo();
        try {
            NotificationCenter.getInstance().postNotification(NotificationCenter.didCameraCountReceived, Camera.getNumberOfCameras());
            mCamera = Camera.open(front ? frontCam : rearCam);
            Camera.Parameters parameters = mCamera.getParameters();
            boolean hasFlash = parameters.getSupportedFlashModes() != null && !parameters.getSupportedFlashModes().isEmpty();
            String stringMode = flash == FlashMode.AUTO ? Camera.Parameters.FLASH_MODE_AUTO : flash == FlashMode.OFF ? Camera.Parameters.FLASH_MODE_OFF : Camera.Parameters.FLASH_MODE_ON;
            if(hasFlash && parameters.getSupportedFlashModes().indexOf(stringMode) >= 0)
                parameters.setFlashMode(stringMode);
            NotificationCenter.getInstance().postNotification(NotificationCenter.didFlashInfoReceived, hasFlash);
            if(BuildConfig.DEBUG)
            {
                for(Camera.Size s : parameters.getSupportedPreviewSizes())
                {
                    Log.d(TAG, String.format("supports %d x %d", s.width, s.height));
                }
            }
            Camera.Size largest = Collections.max(
                    parameters.getSupportedPictureSizes(),
                    new CompareSizesByArea());

            // Find out if we need to swap dimension to get the preview size relative to sensor
            // coordinate.
            int displayRotation = AndroidUtilities.getWindowManager().getDefaultDisplay().getRotation();
            //noinspection ConstantConditions
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(front ? frontCam : rearCam, info);
            int mSensorOrientation = info.orientation;
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("orientation = %d", mSensorOrientation));
            boolean swappedDimensions = false;
            switch (displayRotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_180:
                    if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                        swappedDimensions = true;
                    }
                    break;
                case Surface.ROTATION_90:
                case Surface.ROTATION_270:
                    if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                        swappedDimensions = true;
                    }
                    break;
                default:
                    Log.e(TAG, "Display rotation is invalid: " + displayRotation);
            }
            mCamera.setDisplayOrientation(mSensorOrientation == 270 && front ? 90 : mSensorOrientation);
            m.reset();
            m.postRotate(getOrientation(displayRotation, mSensorOrientation));
            Point displaySize = new Point();
            AndroidUtilities.getWindowManager().getDefaultDisplay().getSize(displaySize);
            int rotatedPreviewWidth = width;
            int rotatedPreviewHeight = height;
            int maxPreviewWidth = displaySize.x;
            int maxPreviewHeight = displaySize.y;

            if (swappedDimensions) {
                rotatedPreviewWidth = height;
                rotatedPreviewHeight = width;
                maxPreviewWidth = displaySize.y;
                maxPreviewHeight = displaySize.x;
            }

            if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                maxPreviewWidth = MAX_PREVIEW_WIDTH;
            }

            if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                maxPreviewHeight = MAX_PREVIEW_HEIGHT;
            }

            mPreviewSize = chooseOptimalSize(parameters.getSupportedPreviewSizes(), rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth, maxPreviewHeight, largest);
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("chosen preview size: %d x %d; largest size: %d x %d", mPreviewSize.width, mPreviewSize.height, largest.width, largest.height));
            parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            parameters.setPictureSize(largest.width, largest.height);
            final int orientation = AppLoader.sharedInstance().getResources().getConfiguration().orientation;
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    try
                    {
                        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            ((AutoFitSurfaceView) attachedTextureView).setAspectRatio(
                                    mPreviewSize.width, mPreviewSize.height);
                        } else {
                            ((AutoFitSurfaceView) attachedTextureView).setAspectRatio(
                                    mPreviewSize.height, mPreviewSize.width);
                        }
                    }
                    catch(Exception ignored) {}
                }
            });
            mCamera.setParameters(parameters);
            activeCam = front ? ActiveCam.FRONT : ActiveCam.REAR;
            mCamera.setPreviewDisplay(((SurfaceView) attachedTextureView).getHolder());
            mCamera.startPreview();
        } catch (Exception e) {
            Log.e(TAG, "failed to open Camera");
            e.printStackTrace();
        }
    }

    @Override
    public void closeCamera() {
        performCloseCamera(null);
    }
    public void performCloseCamera(final Runnable afterClose)
    {
        bgHandler.post(new Runnable() {
            @Override
            public void run() {
                closeCameraRunnable.run();
                if(afterClose != null)
                {
                    AppLoader.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            afterClose.run();
                        }
                    });
                }
            }
        });
    }

    @Override
    public void swapCamera() {
        performCloseCamera(swapCamRunnable);
    }

    @Override
    public void onUiResume() {
        if(attachedTextureView.getWidth() == 0)
            ((SurfaceView) attachedTextureView).getHolder().addCallback(this);
        else
            openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), activeCam == ActiveCam.FRONT);
    }

    @Override
    public void onUiPause(Runnable onFinish) {
        performCloseCamera(onFinish);
    }

    @Override
    public void onSetFlashMode() {
        if(mCamera != null)
        {
            try
            {
                Camera.Parameters params = mCamera.getParameters();
                String stringMode = flash == FlashMode.AUTO ? Camera.Parameters.FLASH_MODE_AUTO : flash == FlashMode.OFF ? Camera.Parameters.FLASH_MODE_OFF : Camera.Parameters.FLASH_MODE_ON;
                if(params.getSupportedFlashModes().indexOf(stringMode) >= 0) {
                    params.setFlashMode(stringMode);
                    mCamera.setParameters(params);
                }
            }
            catch(Exception e ) { e.printStackTrace(); }
        }
    }

    @Override
    public void takePicture() {
        try
        {
            mCamera.autoFocus(af);
        }
        catch(Exception ignored) {
            NotificationCenter.getInstance().postNotification(NotificationCenter.didPictureTaken, null);
        }
    }
    private int getOrientation(int rotation, int mSensorOrientation) {
        // Sensor orientation is 90 for most devices, or 270 for some devices (eg. Nexus 5X)
        // We have to take that into account and rotate JPEG properly.
        // For devices with orientation of 90, we simply return our mapping from ORIENTATIONS.
        // For devices with orientation of 270, we need to rotate the JPEG 180 degrees.
        return (ORIENTATIONS.get(rotation) + mSensorOrientation + 270) % 360;
    }

    private static Camera.Size chooseOptimalSize(List<Camera.Size> choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Camera.Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Camera.Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Camera.Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.width;
        int h = aspectRatio.height;
        for (Camera.Size option : choices) {
            if (option.width <= maxWidth && option.height <= maxHeight &&
                    option.height == option.width * h / w) {
                if (option.width >= textureViewWidth &&
                        option.height >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices.get(0);
        }
    }
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        Log.d(TAG, "surfaceChanged");
        if(mCamera != null)
        {
            try
            {

                Camera.Parameters parameters = mCamera.getParameters();
                Camera.Size largest = Collections.max(
                        parameters.getSupportedPictureSizes(),
                        new CompareSizesByArea());
                parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
                parameters.setPictureSize(largest.width, largest.height);
                attachedTextureView.requestLayout();
                mCamera.setParameters(parameters);

                // Important: Call startPreview() to start updating the preview surface.
                // Preview must be started before you can take a picture.
                mCamera.startPreview();
            }
            catch(Exception e) { e.printStackTrace(); }
        }
        else
            openCamera(width, height, activeCam == ActiveCam.FRONT);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        closeCamera();
    }
    static class CompareSizesByArea implements Comparator<Camera.Size> {

        @Override
        public int compare(Camera.Size lhs, Camera.Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.width * lhs.height -
                    (long) rhs.width * rhs.height);
        }
    }
}
