package com.andremacareno.tcontestproj.compat;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.WindowManager;
import android.widget.Toast;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.AppLoader;
import com.andremacareno.tcontestproj.BuildConfig;
import com.andremacareno.tcontestproj.views.AutoFitTextureView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Andre Macareno on 29.04.2016.
 */
@TargetApi(21)
public class LollipopVideoCam extends VideoCaptureHelper {

    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();
    private String frontCamId = null, rearCamId = null;
    private static final String TAG = "LollipopVideoCam";


    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }
    private final Runnable closeCamRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                StillCaptureHelper.sharedInstance().mCameraOpenCloseLock.acquire();
                closePreviewSession();
                if (null != mCameraDevice) {
                    mCameraDevice.close();
                    mCameraDevice = null;
                }
                if (null != mMediaRecorder) {
                    mMediaRecorder.release();
                    mMediaRecorder = null;
                }
            } catch (Exception e) { e.printStackTrace();} finally {
                StillCaptureHelper.sharedInstance().mCameraOpenCloseLock.release();
            }
        }
    };
    private final Runnable stopLongtapRunnable = new Runnable() {
        @Override
        public void run() {
            stopBackgroundThread();
            StillCaptureHelper.sharedInstance().onUiResume();
        }
    };
    private final Runnable longtapRecRunnable = new Runnable() {
        @Override
        public void run() {
            ((LollipopCam) StillCaptureHelper.sharedInstance()).stopBackgroundThread();
            onUiResume();
        }
    };
    private final Runnable swapCamRunnable = new Runnable() {
        @Override
        public void run() {
            if(StillCaptureHelper.activeCam == StillCaptureHelper.ActiveCam.REAR)
            {
                if (((AutoFitTextureView)attachedTextureView).isAvailable()) {
                    openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), true);
                } else {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "textureView is not available");
                    ((AutoFitTextureView)attachedTextureView).setSurfaceTextureListener(mSurfaceTextureListener);
                }
            }
            else {
                if (((AutoFitTextureView)attachedTextureView).isAvailable()) {
                    openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), false);
                } else {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "textureView is not available");
                    ((AutoFitTextureView)attachedTextureView).setSurfaceTextureListener(mSurfaceTextureListener);
                }
            }
        }
    };

    /**
     * A refernce to the opened {@link android.hardware.camera2.CameraDevice}.
     */
    private CameraDevice mCameraDevice;

    /**
     * A reference to the current {@link android.hardware.camera2.CameraCaptureSession} for
     * preview.
     */
    private CameraCaptureSession mPreviewSession;

    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture,
                                              int width, int height) {
            openCamera(width, height, StillCaptureHelper.activeCam == StillCaptureHelper.ActiveCam.FRONT);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture,
                                                final int width, final int height) {
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    configureTransform(width, height);
                }
            });
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            closeCamera();
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

    };

    /**
     * The {@link android.util.Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * The {@link android.util.Size} of video recording.
     */
    private Size mVideoSize;

    /**
     * MediaRecorder
     */
    private MediaRecorder mMediaRecorder;


    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its status.
     */
    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice cameraDevice) {
            mCameraDevice = cameraDevice;
            startPreview();
            StillCaptureHelper.sharedInstance().mCameraOpenCloseLock.release();
            if (null != attachedTextureView) {
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        configureTransform(attachedTextureView.getWidth(), attachedTextureView.getHeight());
                        synchronized(recording)
                        {
                            if(longtapRecording.get() && !recording.get())
                            {
                                AppLoader.applicationHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        startRecording();
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            StillCaptureHelper.sharedInstance().mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(CameraDevice cameraDevice, int error) {
            StillCaptureHelper.sharedInstance().mCameraOpenCloseLock.release();
            closeCamera();
            //TODO error message
        }

    };
    private Integer mSensorOrientation;
    private String mNextVideoAbsolutePath;
    private CaptureRequest.Builder mPreviewBuilder;
    private Surface mRecorderSurface;

    /**
     * In this sample, we choose a video size with 3x4 aspect ratio. Also, we don't use sizes
     * larger than 1080p, since MediaRecorder cannot handle such a high-resolution video.
     *
     * @param choices The list of available sizes
     * @return The video size
     */
    private static Size chooseVideoSize(Size[] choices) {
        Size max = null;
        for (Size size : choices) {
            // && size.getWidth() <= 1080
            if (size.getWidth() == size.getHeight() * 16 / 9) {
                if(max == null)
                    max = size;
                else if(size.getWidth() * size.getHeight() > max.getWidth() * max.getHeight())
                    max = size;
            }
        }
        if(max != null)
            return max;
        Log.e(TAG, "Couldn't find any suitable video size");
        return choices[choices.length - 1];
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, chooses the smallest one whose
     * width and height are at least as large as the respective requested values, and whose aspect
     * ratio matches with the specified value.
     *
     * @param choices     The list of sizes that the camera supports for the intended output class
     * @param width       The minimum desired width
     * @param height      The minimum desired height
     * @param aspectRatio The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<Size>();
        Size max = null;
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            Log.d(TAG, String.format("%d == %d * %d / %d", option.getHeight(), option.getWidth(), h, w));
            if (option.getHeight() == option.getWidth() * h / w &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
            else if(option.getHeight() == option.getWidth() * h / w && (max == null || max.getHeight() * max.getWidth() < option.getWidth() * option.getHeight()))
                max = option;
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        }
        else if(max != null)
            return max;
        else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }
    @Override
    public void onUiResume() {
        startBackgroundThread();
        if (((AutoFitTextureView)attachedTextureView).isAvailable()) {
            openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), StillCaptureHelper.activeCam == StillCaptureHelper.ActiveCam.FRONT);
        } else {
            ((AutoFitTextureView)attachedTextureView).setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onUiPause(final Runnable onFinish) {
        performCloseCamera(new Runnable() {
            @Override
            public void run() {
                stopBackgroundThread();
                if(onFinish != null)
                    onFinish.run();
            }
        });
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        if(mBackgroundThread == null)
            return;
        try {
            mBackgroundHandler.removeCallbacksAndMessages(null);
            mBackgroundThread.quitSafely();
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tries to open a {@link CameraDevice}. The result is listened by `mStateCallback`.
     */
    private void performOpenCamera(final int width, final int height, boolean front)
    {
        try {
            CameraManager manager = (CameraManager) AppLoader.sharedInstance().getSystemService(Context.CAMERA_SERVICE);
            if((front && frontCamId == null) || (!front && rearCamId == null))
            {
                for(String camId : manager.getCameraIdList())
                {
                    CameraCharacteristics characteristics
                            = manager.getCameraCharacteristics(camId);
                    Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                    if(facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT)
                        frontCamId = camId;
                    else
                        rearCamId = camId;
                }
            }
            String cameraId = front && frontCamId != null ? frontCamId : rearCamId;

            // Choose the sizes for camera preview and video recording
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics
                    .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
            Log.d(TAG, String.format("videoSize = %d x %d", mVideoSize.getWidth(), mVideoSize.getHeight()));
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                    width, height, mVideoSize);
            final int orientation = AppLoader.sharedInstance().getResources().getConfiguration().orientation;
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    configureTransform(width, height);
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        ((AutoFitTextureView)attachedTextureView).setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
                    } else {
                        ((AutoFitTextureView)attachedTextureView).setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
                    }
                }
            });
            //mMediaRecorder = new MediaRecorder();
            manager.openCamera(cameraId, mStateCallback, null);
            StillCaptureHelper.activeCam = front ? StillCaptureHelper.ActiveCam.FRONT : StillCaptureHelper.ActiveCam.REAR;
        } catch (CameraAccessException e) {
            e.printStackTrace();
            Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), "Cannot access the camera.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            //TODO error message
            e.printStackTrace();
        }
        /*catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException("Interrupted while trying to lock camera opening.");
        }*/
    }
    @Override
    protected void openCamera(final int width, final int height, final boolean front) {
        performCloseCamera(new Runnable() {
            @Override
            public void run() {
                if(mBackgroundHandler == null)
                    return;
                mBackgroundHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(BuildConfig.DEBUG)
                            Log.d(TAG, "opening camera in video mode");
                        performOpenCamera(width, height, front);
                    }
                });
            }
        });
    }
    private void performCloseCamera(final Runnable afterClose)
    {
        if(mBackgroundHandler == null)
            return;
        mBackgroundHandler.post(new Runnable() {
            @Override
            public void run() {
                closeCamRunnable.run();
                if(afterClose != null)
                    AppLoader.applicationHandler.post(afterClose);
            }
        });
    }
    @Override
    protected void closeCamera() {
        performCloseCamera(null);
    }

    @Override
    public void swapCamera() {
        performCloseCamera(swapCamRunnable);
    }

    /**
     * Start the camera preview.
     */
    private void startPreview() {
        if (null == mCameraDevice || !((AutoFitTextureView)attachedTextureView).isAvailable() || null == mPreviewSize) {
            return;
        }
        try {
            closePreviewSession();
            SurfaceTexture texture = ((AutoFitTextureView)attachedTextureView).getSurfaceTexture();
            assert texture != null;
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            Surface previewSurface = new Surface(texture);
            mPreviewBuilder.addTarget(previewSurface);

            mCameraDevice.createCaptureSession(Arrays.asList(previewSurface), new CameraCaptureSession.StateCallback() {

                @Override
                public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                    mPreviewSession = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                }
                @Override
                public void onReady(CameraCaptureSession cameraCaptureSession)
                {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "onReady");
                }
            }, mBackgroundHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update the camera preview. {@link #startPreview()} needs to be called in advance.
     */
    private void updatePreview() {
        if (null == mCameraDevice) {
            return;
        }
        try {
            setUpCaptureRequestBuilder(mPreviewBuilder);
            mPreviewSession.setRepeatingRequest(mPreviewBuilder.build(), null, mBackgroundHandler);
        } catch (Exception e) {
            e.printStackTrace();
            /*if(thread != null && thread.isAlive())
                thread.quitSafely();*/
        }
    }

    private void setUpCaptureRequestBuilder(CaptureRequest.Builder builder) {
        builder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
    }

    /**
     * Configures the necessary {@link android.graphics.Matrix} transformation to `mTextureView`.
     * This method should not to be called until the camera preview size is determined in
     * openCamera, or until the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        if (null == attachedTextureView || null == mPreviewSize) {
            return;
        }
        int rotation = ((WindowManager) AppLoader.sharedInstance().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        ((AutoFitTextureView)attachedTextureView).setTransform(matrix);
    }
    private void releaseRecorder()
    {
        if (mMediaRecorder != null) {
            mMediaRecorder.reset();   // clear recorder configuration
            mMediaRecorder.release(); // release the recorder object
            mMediaRecorder = null;
        }
    }
    private void setUpMediaRecorder() throws IOException {
        if(mMediaRecorder != null)
            releaseRecorder();
        mMediaRecorder = new MediaRecorder();
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (mNextVideoAbsolutePath == null || mNextVideoAbsolutePath.isEmpty()) {
            mNextVideoAbsolutePath = getVideoFilePath();
        }
        mMediaRecorder.setOutputFile(mNextVideoAbsolutePath);
        mMediaRecorder.setVideoEncodingBitRate(10000000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        int rotation = ((WindowManager) AppLoader.sharedInstance().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
        switch (mSensorOrientation) {
            case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                mMediaRecorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
                break;
            case SENSOR_ORIENTATION_INVERSE_DEGREES:
                mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
                break;
        }
        mMediaRecorder.prepare();
    }

    private String getVideoFilePath() {
        return AndroidUtilities.getVideoFolder().getAbsolutePath() + "/"
                + System.currentTimeMillis() + ".mp4";
    }

    @Override
    public void startLongtapRecording()
    {
        synchronized(longtapRecording)
        {
            longtapRecording.set(true);
        }
        ((LollipopCam) StillCaptureHelper.sharedInstance()).performCloseCamera(longtapRecRunnable);
    }
    @Override
    public void stopLongtapRecording()
    {
        stopRecording();
    }
    @Override
    protected void startRecording() {
        Log.d(TAG, "stopRecording");
        if (null == mCameraDevice || !((AutoFitTextureView)attachedTextureView).isAvailable() || null == mPreviewSize) {
            Log.d(TAG, "stopRecording: return");
            return;
        }
        try {
            closePreviewSession();
            setUpMediaRecorder();
            SurfaceTexture texture = ((AutoFitTextureView)attachedTextureView).getSurfaceTexture();
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
            mPreviewBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
            List<Surface> surfaces = new ArrayList<>();

            // Set up Surface for the camera preview
            Surface previewSurface = new Surface(texture);
            surfaces.add(previewSurface);
            mPreviewBuilder.addTarget(previewSurface);

            // Set up Surface for the MediaRecorder
            mRecorderSurface = mMediaRecorder.getSurface();
            surfaces.add(mRecorderSurface);
            mPreviewBuilder.addTarget(mRecorderSurface);

            // Start a capture session
            // Once the session starts, we can update the UI and start recording
            mCameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {

                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    mPreviewSession = cameraCaptureSession;
                    updatePreview();
                    AppLoader.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            try
                            {
                                // UI
                                synchronized(recording)
                                {
                                    recording.set(true);
                                }
                                // Start recording
                                mMediaRecorder.start();
                            }
                            catch(Exception e) { e.printStackTrace(); synchronized(recording)
                            {
                                recording.set(false);
                            };
                            }
                        }
                    });
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                }
            }, mBackgroundHandler);
        }
        catch(IOException e)
        {
            e.printStackTrace();
            performStopRecording(true, true);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void closePreviewSession() {
        if (mPreviewSession != null) {
            try
            {
                mPreviewSession.stopRepeating();
                mPreviewSession.abortCaptures();
            }
            catch(Exception e) { e.printStackTrace(); }
            mPreviewSession.close();
            mPreviewSession = null;
        }
    }
    @Override
    protected void stopRecording()
    {
        performStopRecording(false, false);
    }
    private void performStopRecording(boolean failed, boolean dueToException) {
        boolean sendNull = false;
        try {
            if(mPreviewSession != null)
            {
                synchronized(recording)
                {
                    if(longtapRecording.get() && recording.get())
                    {
                        closePreviewSession();
                    }
                    else
                    {
                        mPreviewSession.stopRepeating();
                        mPreviewSession.abortCaptures();
                    }
                }
            }
            if(failed && dueToException) {
                synchronized(recording)
                {
                    recording.set(false);
                }
                throw new Exception("stopRecording called due to MediaRecorder error");
            }
            synchronized(recording)
            {
                if(!recording.get())
                    throw new IllegalStateException("calling stop() before MediaRecorder start");
            }
            // UI
            synchronized(recording)
            {
                recording.set(false);
            }
            // Stop recording
            mMediaRecorder.stop();
            releaseRecorder();
            final String path = mNextVideoAbsolutePath;
            NotificationCenter.getInstance().postNotification(NotificationCenter.didVideoRecorded, path);
            if(mBackgroundHandler != null)
                mBackgroundHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bmp;
                        try
                        {
                            bmp = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);
                            //TODO fade?
                        }
                        catch(Exception e) { e.printStackTrace(); bmp = null; }
                        NotificationCenter.getInstance().postNotification(NotificationCenter.didVideoProcessed, bmp);
                    }
                });
            synchronized(longtapRecording)
            {
                if(!longtapRecording.get())
                    startPreview();
            }
        } catch (Exception e) {
            e.printStackTrace();
            sendNull = true;
        }
        finally {
            mNextVideoAbsolutePath = null;
        }
        if(sendNull)
            NotificationCenter.getInstance().postNotification(NotificationCenter.didVideoRecorded, null);
        synchronized(longtapRecording)
        {
            if(longtapRecording.get())
            {
                longtapRecording.set(false);
                if(sendNull)
                    performCloseCamera(stopLongtapRunnable);
                else
                    onUiPause(null);
            }
        }
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }
}
