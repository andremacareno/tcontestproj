package com.andremacareno.tcontestproj.compat;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.widget.Toast;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tcontestproj.AndroidUtilities;
import com.andremacareno.tcontestproj.AppLoader;
import com.andremacareno.tcontestproj.BuildConfig;
import com.andremacareno.tcontestproj.R;
import com.andremacareno.tcontestproj.views.AutoFitTextureView;
import com.andremacareno.tcontestproj.views.control.FlashMode;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by andremacareno on 27/04/16.
 */
@TargetApi(21)
public class LollipopCam extends StillCaptureHelper {
    private static final String TAG = "LollipopCam";
    private static final int STATE_PREVIEW = 0;
    private static final int STATE_WAITING_LOCK = 1;
    private static final int STATE_WAITING_PRECAPTURE = 2;
    private static final int STATE_WAITING_NON_PRECAPTURE = 3;
    private static final int STATE_PICTURE_TAKEN = 4;
    private static final int MAX_PREVIEW_WIDTH = 1920;
    private static final int MAX_PREVIEW_HEIGHT = 1080;



    private String rearCameraId, frontCameraId;
    private CameraCaptureSession mCaptureSession;
    private CameraDevice mCameraDevice;
    private CaptureRequest.Builder mPreviewRequestBuilder;
    private CaptureRequest mPreviewRequest;
    private ImageReader mImageReader;
    private Size mPreviewSize;
    private int mSensorOrientation;
    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private MediaRecorder mMediaRecorder;
    private int mState = STATE_PREVIEW;
    private int reqWidth = 0, reqHeight = 0;
    private Runnable closeCamRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                mCameraOpenCloseLock.acquire();
                if (null != mCaptureSession) {
                    mCaptureSession.stopRepeating();
                    mCaptureSession.abortCaptures();
                    mCaptureSession.close();
                    mCaptureSession = null;
                }
                if (null != mCameraDevice) {
                    mCameraDevice.close();
                    mCameraDevice = null;
                }
                if (null != mImageReader) {
                    mImageReader.close();
                    mImageReader = null;
                }
            } catch (InterruptedException e) {
                throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
            }
            catch(Exception e) {
                e.printStackTrace();
            }
            finally {
                mCameraOpenCloseLock.release();
            }
        }
    };
    private Runnable swapCamRunnable = new Runnable() {
        @Override
        public void run() {
            if(activeCam == ActiveCam.REAR)
            {
                if (((AutoFitTextureView)attachedTextureView).isAvailable()) {
                    openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), true);
                } else {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "textureView is not available");
                    ((AutoFitTextureView)attachedTextureView).setSurfaceTextureListener(mSurfaceTextureListener);
                }
            }
            else {
                if (((AutoFitTextureView)attachedTextureView).isAvailable()) {
                    openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), false);
                } else {
                    if(BuildConfig.DEBUG)
                        Log.d(TAG, "textureView is not available");
                    ((AutoFitTextureView)attachedTextureView).setSurfaceTextureListener(mSurfaceTextureListener);
                }
            }
        }
    };
    private static final HandlerThread ht = new HandlerThread("CameraOpenHandlerThread");
    static {
        ht.start();
    }
    private static Handler openCameraHandler = new Handler(ht.getLooper());
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "onSurfaceTextureAvailable");
            openCamera(width, height, activeCam == ActiveCam.FRONT);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, final int width, final int height) {
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    configureTransform(width, height);
                }
            });
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            closeCamera();
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
        }

    };
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "onDisconnected");
            mCameraOpenCloseLock.release();
            //CameraManager manager = (CameraManager) AppLoader.sharedInstance().getSystemService(Context.CAMERA_SERVICE);
            //manager.registerAvailabilityCallback(camAvailability, mBackgroundHandler);
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            closeCamera();
            //TODO error message
            /*Activity activity = getActivity();
            if (null != activity) {
                activity.finish();
            }*/
        }

    };
    private final CameraManager.AvailabilityCallback camAvailability = new CameraManager.AvailabilityCallback() {
        @Override
        public void onCameraAvailable(String cameraId) {
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), activeCam == ActiveCam.FRONT);
                    CameraManager manager = (CameraManager) AppLoader.sharedInstance().getSystemService(Context.CAMERA_SERVICE);
                    manager.unregisterAvailabilityCallback(camAvailability);
                }
            });
        }
    };
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener
            = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {
            mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), new File(AppLoader.sharedInstance().getExternalFilesDir(null), "pic.jpg"), reqWidth, reqHeight));
        }

    };

    private void performOpenCamera(final int width, final int height, final boolean front)
    {
        try {
            reqWidth = width;
            reqHeight = height;
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("width = %d; height = %d", width, height));
            if(mMediaRecorder == null)
                mMediaRecorder = new MediaRecorder();
            activeCam = front ? ActiveCam.FRONT : ActiveCam.REAR;
            setUpCameraOutputs(width, height);
            AppLoader.applicationHandler.post(new Runnable() {
                @Override
                public void run() {
                    configureTransform(width, height);
                }
            });
            CameraManager manager = (CameraManager) AppLoader.sharedInstance().getSystemService(Context.CAMERA_SERVICE);
            if (!mCameraOpenCloseLock.tryAcquire(10000, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            manager.openCamera(front ? frontCameraId : rearCameraId, mStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
        catch(SecurityException e) {
            e.printStackTrace();
            Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), R.string.requires_camera_permission, Toast.LENGTH_LONG).show();
            //TODO alertdialog with "go to settings" button
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    @Override
    public void openCamera(final int width, final int height, final boolean front) {
        performCloseCamera(new Runnable() {
            @Override
            public void run() {
                performOpenCamera(width, height, front);
            }
        });
    }

    /**
     * Closes the current {@link CameraDevice}.
     */
    public void performCloseCamera(final Runnable afterClose)
    {
        if(mBackgroundHandler == null)
            return;
        mBackgroundHandler.post(new Runnable() {
            @Override
            public void run() {
                closeCamRunnable.run();
                if(afterClose != null) {
                    AppLoader.applicationHandler.post(afterClose);
                }
            }
        });
    }
    @Override
    public void closeCamera() {
        performCloseCamera(null);
    }

    @Override
    public void onUiResume() {
        if(BuildConfig.DEBUG)
            Log.d(TAG, "onUiResume");
        startBackgroundThread();

        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (((AutoFitTextureView)attachedTextureView).isAvailable()) {
            openCamera(attachedTextureView.getWidth(), attachedTextureView.getHeight(), activeCam == ActiveCam.FRONT);
        } else {
            if(BuildConfig.DEBUG)
                Log.d(TAG, "textureView is not available");
            ((AutoFitTextureView)attachedTextureView).setSurfaceTextureListener(mSurfaceTextureListener);
        }

    }

    @Override
    public void onUiPause(final Runnable onFinish) {
        performCloseCamera(new Runnable() {
            @Override
            public void run() {
                stopBackgroundThread();
                if(onFinish != null)
                    onFinish.run();
            }
        });
    }


    private void setUpCameraOutputs(int width, int height) {
        CameraManager manager = (CameraManager) AppLoader.sharedInstance().getSystemService(Context.CAMERA_SERVICE);
        try {
            NotificationCenter.getInstance().postNotification(NotificationCenter.didCameraCountReceived, manager.getCameraIdList().length);
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics
                        = manager.getCameraCharacteristics(cameraId);

                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);

                StreamConfigurationMap map = characteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }
                boolean isFront = false;
                if(facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    frontCameraId = cameraId;
                    isFront = true;
                }
                else
                    rearCameraId = cameraId;
                Size largest;
                if(activeCam == ActiveCam.FRONT && isFront || (activeCam == ActiveCam.REAR && !isFront))
                {
                    // For still image captures, we use the largest available size.
                    largest = Collections.max(
                            Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                            new CompareSizesByArea());
                    mImageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(),
                            ImageFormat.JPEG, 2);
                    mImageReader.setOnImageAvailableListener(
                            mOnImageAvailableListener, mBackgroundHandler);
                }
                else
                    continue;

                // Find out if we need to swap dimension to get the preview size relative to sensor
                // coordinate.
                int displayRotation = AndroidUtilities.getWindowManager().getDefaultDisplay().getRotation();
                //noinspection ConstantConditions
                mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                boolean swappedDimensions = false;
                switch (displayRotation) {
                    case Surface.ROTATION_0:
                    case Surface.ROTATION_180:
                        if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                            swappedDimensions = true;
                        }
                        break;
                    case Surface.ROTATION_90:
                    case Surface.ROTATION_270:
                        if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                            swappedDimensions = true;
                        }
                        break;
                    default:
                        Log.e(TAG, "Display rotation is invalid: " + displayRotation);
                }
                Point displaySize = new Point();
                AndroidUtilities.getWindowManager().getDefaultDisplay().getSize(displaySize);
                int rotatedPreviewWidth = width;
                int rotatedPreviewHeight = height;
                int maxPreviewWidth = displaySize.x;
                int maxPreviewHeight = displaySize.y;

                if (swappedDimensions) {
                    rotatedPreviewWidth = height;
                    rotatedPreviewHeight = width;
                    maxPreviewWidth = displaySize.y;
                    maxPreviewHeight = displaySize.x;
                }

                if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
                }

                if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                }

                // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                // garbage capture data.
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                        maxPreviewHeight, largest);
                // We fit the aspect ratio of TextureView to the size of preview we picked.
                final int orientation = AppLoader.sharedInstance().getResources().getConfiguration().orientation;
                AppLoader.applicationHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            ((AutoFitTextureView)attachedTextureView).setAspectRatio(
                                    mPreviewSize.getWidth(), mPreviewSize.getHeight());
                        } else {
                            ((AutoFitTextureView)attachedTextureView).setAspectRatio(
                                    mPreviewSize.getHeight(), mPreviewSize.getWidth());
                        }
                    }
                });

                // Check if the flash is supported.
                Boolean available = characteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
                hasFlash = available == null ? false : available;
                NotificationCenter.getInstance().postNotification(NotificationCenter.didFlashInfoReceived, hasFlash);
            }
        } catch (Exception e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            //TODO error message
            e.printStackTrace();
        }
    }
    private void configureTransform(int viewWidth, int viewHeight) {
        if (null == attachedTextureView || null == mPreviewSize) {
            return;
        }
        int rotation = AndroidUtilities.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        ((AutoFitTextureView)attachedTextureView).setTransform(matrix);
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    public void stopBackgroundThread() {
        if(mBackgroundThread == null)
            return;
        try {
            mBackgroundHandler.removeCallbacksAndMessages(null);
            mBackgroundThread.quitSafely();
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }
    private void createCameraPreviewSession() {
        try {
            SurfaceTexture texture = ((AutoFitTextureView)attachedTextureView).getSurfaceTexture();

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder
                    = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous for camera preview.
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

                                if(flash == FlashMode.OFF) {
                                    mPreviewRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_OFF);
                                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
                                }
                                else if(flash == FlashMode.ON) {
                                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
                                }
                                else
                                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
                                // Finally, we start displaying the camera preview.
                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest,
                                        mCaptureCallback, mBackgroundHandler);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            //Toast.makeText(AppLoader.sharedInstance().getApplicationContext(), R.string.capture_failed, Toast.LENGTH_SHORT).show();
                        }
                    }, null
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSetFlashMode()
    {
        if(mPreviewRequestBuilder == null)
            return;
        if(hasFlash)
        {
            if(flash == FlashMode.OFF) {
                mPreviewRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_OFF);
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
            }
            else if(flash == FlashMode.ON) {
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
            }
            else
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
            mPreviewRequest = mPreviewRequestBuilder.build();
            try
            {
                mCaptureSession.setRepeatingRequest(mPreviewRequest,
                        mCaptureCallback, mBackgroundHandler);
            }
            catch(Exception e) { e.printStackTrace(); }
        }
    }

    private CameraCaptureSession.CaptureCallback mCaptureCallback
            = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {
            switch (mState) {
                case STATE_PREVIEW: {
                    // We have nothing to do when the camera preview is working normally.
                    break;
                }
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    if (afState == null) {
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            mState = STATE_PICTURE_TAKEN;
                            captureStillPicture();
                        } else {
                            runPrecaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE;
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        mState = STATE_PICTURE_TAKEN;
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };
    private void runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void captureStillPicture() {
        try {
            if (null == mCameraDevice) {
                return;
            }
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder =
                    mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(mImageReader.getSurface());

            // Use the same AE modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            //Flash
            //captureBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_SINGLE);
            if(hasFlash)
            {
                if(flash == FlashMode.OFF) {
                    captureBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_OFF);
                    captureBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
                }
                else if(flash == FlashMode.ON) {
                    captureBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_SINGLE);
                    captureBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
                }
                else
                    captureBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);


            }

            // Orientation
            int rotation = AndroidUtilities.getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientation(rotation));

            CameraCaptureSession.CaptureCallback CaptureCallback
                    = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
                    //showToast("Saved: " + mFile);
                    //Log.d(TAG, mFile.toString());
                    AppLoader.applicationHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            unlockFocus();
                        }
                    });
                }
            };

            mCaptureSession.stopRepeating();
            mCaptureSession.capture(captureBuilder.build(), CaptureCallback, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void takePicture() {
        lockFocus();
    }


    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        try {
            // This is how to tell the camera to lock focus.
            if(activeCam != ActiveCam.FRONT) {
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                        CameraMetadata.CONTROL_AF_TRIGGER_START);
                // Tell #mCaptureCallback to wait for the lock.
                mState = STATE_WAITING_LOCK;
                mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                        mBackgroundHandler);
            }
            else
                captureStillPicture();          //front cameras are usually fixed-focusing
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
            // After this, the camera will go back to the normal state of preview.
            mState = STATE_PREVIEW;
            mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback,
                    mBackgroundHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private int getOrientation(int rotation) {
        // Sensor orientation is 90 for most devices, or 270 for some devices (eg. Nexus 5X)
        // We have to take that into account and rotate JPEG properly.
        // For devices with orientation of 90, we simply return our mapping from ORIENTATIONS.
        // For devices with orientation of 270, we need to rotate the JPEG 180 degrees.
        return (ORIENTATIONS.get(rotation) + mSensorOrientation + 270) % 360;
    }
    public void swapCamera() {
        performCloseCamera(swapCamRunnable);
    }

    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }
    }



    private static class ImageSaver implements Runnable {

        /**
         * The JPEG image
         */
        private final Image mImage;
        /**
         * The file we save the image into.
         */
        private final File mFile;
        private final int reqWidth, reqHeight;
        public ImageSaver(Image image, File file, int reqWidth, int reqHeight) {
            mImage = image;
            mFile = file;
            this.reqWidth = reqWidth;
            this.reqHeight = reqHeight;
        }

        @Override
        public void run() {
            if(BuildConfig.DEBUG)
                Log.d(TAG, String.format("width = %d; height = %d", mImage.getWidth(), mImage.getHeight()));
            final int picWidth = mImage.getWidth();
            final int picHeight = mImage.getHeight();
            ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            /*boolean success = false;
            FileOutputStream output = null;
            try {
                output = new FileOutputStream(mFile);
                output.write(bytes);
                success = true;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mImage.close();
                if (null != output) {
                    try {
                        output.close();
                        NotificationCenter.getInstance().postNotification(NotificationCenter.didTakenPictureTemporarySaved, mFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }*/
            try
            {
                if(reqWidth != 0 && reqHeight != 0)
                {
                    BitmapFactory.Options opts = new BitmapFactory.Options();
                    opts.inJustDecodeBounds = false;
                    opts.inSampleSize = calculateInSampleSize(picWidth, picHeight);
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opts);
                    NotificationCenter.getInstance().postNotification(NotificationCenter.didPictureTaken, bmp);
                }
            }
            finally {
                mImage.close();
                buffer.clear();
                bytes = null;
            }
        }

    }

}
