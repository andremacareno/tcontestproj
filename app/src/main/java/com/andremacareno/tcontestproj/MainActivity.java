package com.andremacareno.tcontestproj;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.andremacareno.asynccore.notificationcenter.NotificationCenter;
import com.andremacareno.tcontestproj.screens.CameraFragment;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private FrameLayout container;
    private BackPressListener backPressInterceptor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        container = new FrameLayout(this);
        container.setId(R.id.container);
        setContentView(container);
        final View decorView = getWindow().getDecorView();
        if(Build.VERSION.SDK_INT >= 19)
        {
            decorView.setOnSystemUiVisibilityChangeListener (new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        decorView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                    }
                }});

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_IMMERSIVE);

        }
        else if(Build.VERSION.SDK_INT >= 16)
        {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
        if(savedInstanceState == null) {
            requestCameraPermission();
        }
    }
    private void openCameraFragment()
    {
        Fragment f = new CameraFragment();
        f.setRetainInstance(true);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.container, f, Constants.CAMERA_FRAGMENT).commitAllowingStateLoss();
        fm.executePendingTransactions();
    }
    public void setKeepScreenOn(boolean keep)
    {
        if(keep)
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        else
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if(BuildConfig.DEBUG)
        {
            Log.d("MainActivity", String.format("requested permissions: %s", Arrays.toString(permissions)));
            Log.d("MainActivity", String.format("grantResults: %s", Arrays.toString(grantResults)));
        }
        if (requestCode == Constants.REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length != 3 && (grantResults[0] != grantResults[2] || grantResults[1] != grantResults[2] || grantResults[0] != grantResults[1] || grantResults[0] != PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(getApplicationContext(), R.string.requires_camera_permission, Toast.LENGTH_LONG).show();
                finish();
            }
            else
                openCameraFragment();
        }
        else if(requestCode == Constants.REQUEST_STORAGE_PERMISSION)
        {
            if(BuildConfig.DEBUG)
                Log.d("MainActivity", "storage permission requested");
            if (grantResults.length != 0 && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                if(BuildConfig.DEBUG)
                    Log.d("MainActivity", "posting notification");
                NotificationCenter.getInstance().postNotification(NotificationCenter.didStoragePermissionGranted);
            }
            else
                Toast.makeText(getApplicationContext(), R.string.requires_storage_permission, Toast.LENGTH_LONG).show();
        }
        else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    private void requestCameraPermission() {
        final String[] permissions = new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.cam_permission_request)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    permissions,
                                    Constants.REQUEST_CAMERA_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    MainActivity.this.finish();
                                }
                            })
                    .create();
        }
        else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.cam_permission_request)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    permissions,
                                    Constants.REQUEST_CAMERA_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    MainActivity.this.finish();
                                }
                            })
                    .create();
        }
        else if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.storage_permission_request)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    permissions,
                                    Constants.REQUEST_CAMERA_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    MainActivity.this.finish();
                                }
                            })
                    .create();
        }
        else {
            ActivityCompat.requestPermissions(this, permissions,
                    Constants.REQUEST_CAMERA_PERMISSION);
        }
    }
    public void requestStoragePermission()
    {
        final String[] permissions = Build.VERSION.SDK_INT >= 16 ? new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE} : new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.storage_permission_request)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    permissions,
                                    Constants.REQUEST_STORAGE_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //MainActivity.this.finish();
                                }
                            })
                    .create();
        }
        else {
            ActivityCompat.requestPermissions(this, permissions,
                    Constants.REQUEST_STORAGE_PERMISSION);
        }
    }
    public void removeBackPressInterceptor(BackPressListener listener)
    {
        if(listener == null)
            return;
        if(listener == this.backPressInterceptor)
            this.backPressInterceptor = null;
    }
    public void interceptBackPress(BackPressListener interceptor) {
        this.backPressInterceptor = interceptor;
    }
    @Override
    public void onBackPressed()
    {
        if(backPressInterceptor != null)
            backPressInterceptor.onBackPressed();
        else
            super.onBackPressed();
    }
}
