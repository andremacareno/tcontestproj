package com.andremacareno.asynccore.notificationcenter;

/**
 * Created by andremacareno on 24/04/15.
 */
public class Notification {
    private int id;
    private Object object;
    public Notification(int id, Object o)
    {
        this.id = id;
        this.object = o;
    }
    public int getId() { return this.id; }
    public Object getObject() { return this.object; };
}
