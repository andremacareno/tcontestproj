package com.andremacareno.asynccore.notificationcenter;


import com.andremacareno.tcontestproj.AppLoader;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by andremacareno on 24/04/15.
 */
public class NotificationCenter {
    private static final String TAG = "NotificationCenter";
    private static int totalEvents = 1;
    public static final int didPictureTaken = totalEvents++;
    public static final int didStoragePermissionGranted = totalEvents++;
    public static final int didTakenPictureTemporarySaved = totalEvents++;
    public static final int didVideoRecorded = totalEvents++;
    public static final int didVideoProcessed = totalEvents++;
    public static final int didFlashInfoReceived = totalEvents++;
    public static final int didCameraCountReceived = totalEvents++;

    private ConcurrentHashMap<Integer, CopyOnWriteArrayList<NotificationObserver>> observers = new ConcurrentHashMap<Integer, CopyOnWriteArrayList<NotificationObserver>>();
    private static volatile NotificationCenter _instance;

    private NotificationCenter() {}
    public static NotificationCenter getInstance()
    {
        if(_instance == null)
        {
            synchronized(NotificationCenter.class)
            {
                if(_instance == null)
                    _instance = new NotificationCenter();
            }
        }
        return _instance;
    }
    public static void stop()
    {
        if(_instance != null)
        {
            synchronized(NotificationCenter.class)
            {
                if(_instance != null) {
                    _instance.removeAllObservers();
                    _instance = null;
                }
            }
        }
    }

    public void postNotification(int id, Object obj)
    {
        Notification notification = new Notification(id, obj);
        CopyOnWriteArrayList<NotificationObserver> observersOfId = observers.get(id);
        if(observersOfId == null)
            return;
        for(NotificationObserver obs : observersOfId)
        {
            obs.didNotificationReceived(notification);
        }
    }
    public void postNotification(int id)
    {
        postNotification(id, null);
    }
    public void postNotificationDelayed(final int id, final Object obj, long delay)
    {
        try
        {
            AppLoader.applicationHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    postNotification(id, obj);
                }
            }, delay);
        }
        catch(Exception ignored) {}
    }
    public void addObserver(NotificationObserver obs) {
        if(obs == null)
            return;
        int id = obs.getId();
        CopyOnWriteArrayList<NotificationObserver> idObservers = observers.get(id);
        if(idObservers == null)
            observers.put(id, (idObservers = new CopyOnWriteArrayList<NotificationObserver>()));
        idObservers.add(obs);
    }

    public void removeObserver(NotificationObserver obs) {
        if(obs == null)
            return;
        int id = obs.getId();
        CopyOnWriteArrayList<NotificationObserver> idObservers = observers.get(id);
        if(idObservers != null)
        {
            idObservers.remove(obs);
            if(idObservers.size() == 0)
                observers.remove(id);
        }
    }
    public void removeAllObservers()
    {
        for(Integer i : observers.keySet())
            observers.get(i).clear();
        observers.clear();
    }

}
