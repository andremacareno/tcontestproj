package com.andremacareno.asynccore;

import android.os.Process;
import android.support.v4.util.LongSparseArray;
import android.util.Log;

import com.andremacareno.tcontestproj.BuildConfig;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Andrew on 09.06.2014.
 */
public class BackgroundTaskManager extends Thread{
    private static volatile BackgroundTaskManager _instance;

    private BackgroundTaskExecutor executor;
    private final AtomicLong nextId = new AtomicLong(0);
    public final LongSparseArray<BackgroundTask> runningTasks = new LongSparseArray<>();
    public BackgroundTaskManager()
    {
        executor = new BackgroundTaskExecutor();
    }

    @Override
    public void run()
    {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        executor.run();
    }
    public void addTask(BackgroundTask t)
    {
        synchronized(nextId)
        {
            long taskId = nextId.getAndIncrement();
            synchronized(runningTasks)
            {
                t.assignTaskId(taskId);
                runningTasks.put(taskId, t);
            }
        }
        executor.addTask(t);
        if(!this.isAlive())
            this.start();
    }
    public void pauseThread()
    {
        if(BuildConfig.DEBUG)
            Log.d("BackgroundTasks", "pause requested");
        synchronized(executor.paused)
        {
            executor.paused.set(true);
        }
    }
    public void unpauseThread() {
        if(BuildConfig.DEBUG)
            Log.d("BackgroundTasks", "unpause requested");
        synchronized(executor.paused)
        {
            executor.paused.set(false);
        }
        synchronized(executor.lockObject)
        {
            executor.lockObject.notifyAll();
        }
        unpauseRunning();
    }


    public static BackgroundTaskManager sharedInstance()
    {
        if(_instance == null)
        {
            synchronized (BackgroundTaskManager.class)
            {
                if(_instance == null)
                    _instance = new BackgroundTaskManager();
            }
        }
        return _instance;
    }
    public static void stopManager()
    {
        if(_instance != null)
        {
            synchronized(BackgroundTaskManager.class)
            {
                if(_instance != null)
                {
                    _instance.interrupt();
                    _instance.executor = null;
                    _instance = null;
                }
            }
        }
    }
    public boolean checkPauseRequested()
    {
        synchronized(executor.paused)
        {
            return executor.paused.get();
        }
    }
    private void unpauseRunning()
    {
        try
        {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    synchronized(runningTasks)
                    {
                        for(int i = 0; i < runningTasks.size(); i++)
                        {
                            try
                            {
                                BackgroundTask task = runningTasks.get(runningTasks.keyAt(i));
                                task.unpause();
                            }
                            catch(Exception ignored) {}
                        }
                    }
                }
            }).start();
        }
        catch(Exception e) { e.printStackTrace(); }
    }
    public void didTaskFinished(long id)
    {
        synchronized(runningTasks)
        {
            runningTasks.remove(id);
        }
    }
}
